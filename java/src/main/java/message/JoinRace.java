/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package message;

import dataholder.createOrJoinRace.BotId;

/**
 *
 * @author Christoffer
 */
public class JoinRace extends SendMessage {

    public final BotId botId;
    public final String trackName;
    public final String password;
    public int carCount;

    public JoinRace(String botName, String botKey, String trackName, String password, int carCount) {
        this.botId = new BotId();
        this.botId.setName(botName);
        this.botId.setKey(botKey);

        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    /**
     * Try and join a random race, if non is found it'll create a new one. If
     * you want to join a private race, check @JoinRace(String botName, String
     * botKey, String trackName, String password, int carCount)
     *
     * @param botName
     * @param botKey
     * @param carCount
     */
    public JoinRace(String botName, String botKey, int carCount) {
        this(botName, botKey, null, null, carCount);
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }

}
