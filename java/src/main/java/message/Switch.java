package message;

/**
 *
 * @author Lasse Lybeck
 */
public class Switch extends SendMessage {

    public final Direction direction;

    public Switch(Direction direction) {
        this.direction = direction;
    }

    @Override
    protected Object msgData() {
        return direction.toString();
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }

    public static enum Direction {

        RIGHT, LEFT;

        @Override
        public String toString() {
            switch (this) {
                case RIGHT:
                    return "Right";
                case LEFT:
                    return "Left";
                default:
                    throw new AssertionError();
            }
        }
    }
}
