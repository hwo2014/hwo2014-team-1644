package message;

/**
 *
 * @author Lasse Lybeck
 */
public class MessageWrapper {

    public final String msgType;
    public final Object data;

    MessageWrapper(String msgType, Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MessageWrapper(final SendMessage sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}
