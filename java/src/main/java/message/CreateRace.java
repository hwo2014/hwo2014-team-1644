/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package message;

import dataholder.createOrJoinRace.BotId;

/**
 *
 * @author Christoffer
 */
public class CreateRace extends SendMessage {

    public final BotId botId;
    public final String trackName;
    public final String password;
    public final int carCount;

    public CreateRace(String botName, String botKey, String trackName, String password, int carCount) {
        this.botId = new BotId();
        this.botId.setKey(botKey);
        this.botId.setName(botName);

        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }

}
