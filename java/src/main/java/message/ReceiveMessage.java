package message;

import com.google.gson.JsonElement;

/**
 *
 * @author Lasse Lybeck
 */
public class ReceiveMessage {

    private String messageType;
    private JsonElement data;
    private Integer gameTick;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public JsonElement getData() {
        return data;
    }

    public void setData(JsonElement data) {
        this.data = data;
    }

    public Integer getGameTick() {
        return gameTick;
    }

    public void setGameTick(int gameTick) {
        this.gameTick = gameTick;
    }
}
