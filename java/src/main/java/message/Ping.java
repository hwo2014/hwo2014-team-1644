package message;

/**
 *
 * @author Lasse Lybeck
 */
public class Ping extends SendMessage {

    @Override
    protected String msgType() {
        return "ping";
    }
}
