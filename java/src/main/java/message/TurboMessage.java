package message;

/**
 *
 * @author Lasse Lybeck
 */
public class TurboMessage extends SendMessage {

    @Override
    protected Object msgData() {
        return "No more crawling!!";
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}
