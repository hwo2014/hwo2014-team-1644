package message;

import com.google.gson.Gson;

/**
 *
 * @author Lasse Lybeck
 */
public abstract class SendMessage {

    public String toJson() {
        return new Gson().toJson(new MessageWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}
