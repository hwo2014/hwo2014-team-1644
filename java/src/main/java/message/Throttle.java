package message;

/**
 *
 * @author Lasse Lybeck
 */
public class Throttle extends SendMessage {

    private double value;

    public Throttle(double value) {
        if (value < 0.0) {
            System.err.println("WARNING! Tried to send throttle value: " + value);
            this.value = 0.0;
        } else if (value > 1.0) {
            System.err.println("WARNING! Tried to send throttle value: " + value);
            this.value = 1.0;
        } else {
            this.value = value;
        }
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}
