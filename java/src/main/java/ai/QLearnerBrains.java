/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ai;

// Harjoitukset tehtävä Jeremias Berg

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class QLearnerBrains implements Serializable {
    private final double[][] qLearningData;
    private final int[][] numberOfTimesInState;
    private int lifeTime;
    private int NUM_RACES;
         
        // Diskretisation of state;
    private final double BUCKETSIZE_DISTANCE_TO_CURVE;  //= 20;
    private final int NUM_DISTANCE_TO_CURVE_BUCKETS = 10;

    private final double BUCKETSIZE_ANGLE_OF_NEXT_CURVE;  //= 5;
    private final int NUM_ANGLE_OF_NEXT_CURVE_BUCKETS = 10;

    private final double BUCKETSIZE_SPEED; // = 0.5; // = 0.5;
    private final int NUM_SPEED_BUCKETS = 14;
    private final double MINIMUM_SPEED = 3;

    private final double BUCKETSIZE_ANGLE; // = 1;
    private final int NUM_ANGLE_BUCKETS = 30;
    private final double MINIMUM_ANGLE = 0;
    
    private final double BUCKETSIZE_RADIUS; // = 20;
    private final int NUM_RADIUS_BUCKETS = 10;
    
    private final int NUM_OF_DIFFERENT_SIGN_BUCKETS = 2;
    
    private final double BASE_FOR_REWARD;
    private final double FACTOR_FUTURE_REWARDS = 0.9;      
   private final int NUM_STEPS_BETWEEN_TEMP_UPDATES = 10000;
    
    private static final int DEFAULT_THROTTLE = 3;
    private double MAX_EXPLORE;

    private boolean lastLaps;

    public QLearnerBrains(double BUCKETSIZE_DISTANCE_TO_CURVE, double BUCKETSIZE_ANGLE_OF_NEXT_CURVE, double BUCKETSIZE_SPEED, double BUCKETSIZE_ANGLE, double BUCKETSIZE_RADIUS, double BASE_FOR_REWARD, int NUM_OF_ACTIONS) {
        this.BUCKETSIZE_DISTANCE_TO_CURVE = BUCKETSIZE_DISTANCE_TO_CURVE;
        this.BUCKETSIZE_ANGLE_OF_NEXT_CURVE = BUCKETSIZE_ANGLE_OF_NEXT_CURVE;
        this.BUCKETSIZE_SPEED = BUCKETSIZE_SPEED;
        this.BUCKETSIZE_ANGLE = BUCKETSIZE_ANGLE;
        this.BUCKETSIZE_RADIUS = BUCKETSIZE_RADIUS;
        this.BASE_FOR_REWARD = BASE_FOR_REWARD;
        this.qLearningData = new double[getNumberOfStates()][NUM_OF_ACTIONS];
        this.numberOfTimesInState = new int[getNumberOfStates()][NUM_OF_ACTIONS];
        this.MAX_EXPLORE = 10E10;
        this.lastLaps = false;
        restartLearning();
    }
    public void unsetLastLaps() {
         lastLaps = false;
    }
    
    public void setFinalLaps() {
       lastLaps = true;
    }
    
    public void addLifetime() {
        lifeTime++;
    }
    
    public void addNumRaces() {
        NUM_RACES++;
    }

    public int getNumRaces() {
        return NUM_RACES;
    }
    
    public int[] lobotomize() {
        boolean needsSwitch = lastLaps;
        if (needsSwitch) {
            unsetLastLaps();
        }
        System.out.println("Total states " + getNumberOfStates());
        int[] toReturn = new int[getNumberOfStates()];
        for (int i = 0 ; i < toReturn.length ; i++) {
            if ( i % 1000 == 0) {
                System.out.println(" At state " + i);
            }
            toReturn[i] = findMax(i);
            
        }
        if (needsSwitch) {
            setFinalLaps();
        }     
        return toReturn;
    }
    
    
    private int getNumberOfStates() {
        return  NUM_DISTANCE_TO_CURVE_BUCKETS * 
                NUM_ANGLE_OF_NEXT_CURVE_BUCKETS * 
                NUM_SPEED_BUCKETS * NUM_ANGLE_BUCKETS *  
                NUM_RADIUS_BUCKETS * NUM_OF_DIFFERENT_SIGN_BUCKETS;
    }
        /**
     * If there's reason to believe a dramatic change in learning conditions This method can be used for reseting all
     * learning parameters. The effect of this on the AI is that it retains all info it has collected, but also starts
     * learning from scratch.
     */
    public final void restartLearning() {
        this.lifeTime = 0;
        for (int[] numberOfTimesInState1 : numberOfTimesInState) {
            for (int j = 0; j < numberOfTimesInState1.length; j++) {
                numberOfTimesInState1[j] = 1;
            }
        }
    }
    
    public int getStateIndex(State s) {
        int distanceIndex = getCorrectIndex(s.getDistanceToNextCurve(), NUM_DISTANCE_TO_CURVE_BUCKETS, BUCKETSIZE_DISTANCE_TO_CURVE);
        int angleOfCurveIndex = getCorrectIndex(s.getAngleOfNextCurve(), NUM_ANGLE_OF_NEXT_CURVE_BUCKETS, BUCKETSIZE_ANGLE_OF_NEXT_CURVE);
        int speedIndex = s.getSpeed() < MINIMUM_SPEED ? 0 : getCorrectIndex(s.getSpeed() - MINIMUM_SPEED, NUM_SPEED_BUCKETS, BUCKETSIZE_SPEED);
        int angleIndex = s.getAngle() < MINIMUM_ANGLE ? 0 : getCorrectIndex(s.getAngle() - MINIMUM_ANGLE, NUM_ANGLE_BUCKETS, BUCKETSIZE_ANGLE);
        int radiusIndex = getCorrectIndex(s.getRadiusOfNextCurve(), NUM_RADIUS_BUCKETS, BUCKETSIZE_RADIUS);
        int signIndex = s.isDifferentAngleSign() ? 1 : 0;
        
        
        return distanceIndex *  NUM_ANGLE_OF_NEXT_CURVE_BUCKETS * 
                                NUM_SPEED_BUCKETS * 
                                NUM_ANGLE_BUCKETS * 
                                NUM_RADIUS_BUCKETS * 
                                NUM_OF_DIFFERENT_SIGN_BUCKETS
                + angleOfCurveIndex *   NUM_SPEED_BUCKETS * 
                                        NUM_ANGLE_BUCKETS * 
                                        NUM_RADIUS_BUCKETS * 
                                        NUM_OF_DIFFERENT_SIGN_BUCKETS
                + speedIndex  *         NUM_ANGLE_BUCKETS * 
                                        NUM_RADIUS_BUCKETS * 
                                        NUM_OF_DIFFERENT_SIGN_BUCKETS
                + angleIndex *          NUM_RADIUS_BUCKETS * 
                                        NUM_OF_DIFFERENT_SIGN_BUCKETS
                + radiusIndex *         NUM_OF_DIFFERENT_SIGN_BUCKETS
                + signIndex;
    }
    
    
    Iterable<State> getHigherAngles(State s) {
        ArrayList<State> states = new ArrayList<>();
        double angle;
        angle = Math.max(MINIMUM_ANGLE, s.getAngle());
        
        while ((angle + BUCKETSIZE_ANGLE) <  (BUCKETSIZE_ANGLE * NUM_ANGLE_BUCKETS + MINIMUM_ANGLE)) {    
            angle += BUCKETSIZE_ANGLE;
            states.add(new State(s.getSpeed(), angle, s.getDistanceToNextCurve(), s.getAngleOfNextCurve(), s.getRadiusOfNextCurve(), s.getLap(), s.getCurAngl(), 0));      
        }        
        return states;
    }

    Iterable<State> getHigherSpeeds(State s) {
        ArrayList<State> states = new ArrayList<>();
        double speed;

        speed = Math.max(MINIMUM_SPEED, s.getSpeed());


        while ((speed + BUCKETSIZE_SPEED) <  (BUCKETSIZE_SPEED * NUM_SPEED_BUCKETS + MINIMUM_SPEED)) {            
            speed += BUCKETSIZE_SPEED;
            states.add(new State(speed, s.getAngle(), s.getDistanceToNextCurve(), s.getAngleOfNextCurve(), s.getRadiusOfNextCurve(), s.getLap(), s.getCurAngl()));      
        }        
        return states;
    }    
    

    private int getCorrectIndex(double val, int numBuckets, double sizeOfBucket) {
        // to fix rounding
        double toAdd = sizeOfBucket / 2;
        //assumes that casting to an int works like floor (i.e (int)(x.abcd... = x) 
        return Math.min(numBuckets - 1, Math.max(0, (int) ((val + toAdd) / sizeOfBucket)));

    }

      /* 
        USE ONLY FOR DEBUGGING
    */
    public void summariseQLearningMatrix() {
        System.out.println("Summarising q learning matrix" );
        System.out.println("Num States " + getNumberOfStates());
        int numZeros = 0;
        double max = qLearningData[0][0]; 
        double min = qLearningData[0][0]; 
        for (int i = 0 ; i < qLearningData.length ; i++) {
            for (int j = 0 ; j < qLearningData[i].length ; j++) {
                if (qLearningData[i][j] < 0) {
                    System.out.println("Found  negative val " + i + "/" + j + " val: " + qLearningData[i][j]);
                }              
                if(qLearningData[i][j] == 0)  {
                     numZeros++;
                }
                if (Double.isInfinite(qLearningData[i][j]) || Double.isNaN(qLearningData[i][j]))
                    System.out.println("Bad value "  + i + "/" + j);

                max = Math.max(max, qLearningData[i][j]);
                min =Math.min(qLearningData[i][j], min);
            }            
        }
        System.out.println("Num zeros: " + numZeros);
        System.out.println("Max " + max);
        System.out.println("Min " + min);
    }
    
   //Debugging
    public void printNumRaces() {
       System.out.println("Num Races " + NUM_RACES);

    }

    public double getReward(double curSpeed) {
            return Math.pow(BASE_FOR_REWARD, Math.abs(curSpeed));
    }

    private double getAlpha(int prevState, int prevAction) {
            double alph;
            if (numberOfTimesInState[prevState][prevAction] != 1) {
                alph =(double)1 / (double)(numberOfTimesInState[prevState][prevAction] / 2);
            }
            else {
                alph = 1;
            }
            QLearning.debugPrint("Alpha " + alph);
            QLearning.debugPrint("Num in state " + numberOfTimesInState[prevState][prevAction]);
            if (Integer.MAX_VALUE - numberOfTimesInState[prevState][prevAction] > 1) {
                numberOfTimesInState[prevState][prevAction] += 1;
            }
            return alph;
    } 
    
    
    public double getMaxQValOnRow(int thisStIndex) {
        double max = qLearningData[thisStIndex][0];
        for (double val : qLearningData[thisStIndex]) {
            if (val > max && val != 0) {
                max = val;
            }
        }
        return max;
    }

    public void updateTableVals(int thisStIndex, int previousStateIndex, int previousActionIndex, double reward) {
        double alphaVal = getAlpha(previousStateIndex, previousActionIndex); 
        double futureReward = getMaxQValOnRow(thisStIndex);
        double totalReward = (1 - alphaVal) * qLearningData[previousStateIndex][previousActionIndex]
                + alphaVal * (reward + FACTOR_FUTURE_REWARDS * futureReward);    
        QLearning.debugPrint("Val before " + qLearningData[previousStateIndex][previousActionIndex]);
        QLearning.debugPrint("Reward " + reward);

//        if (futureReward < 0 || totalReward > qLearningData[previousStateIndex][previousActionIndex]) {
            qLearningData[previousStateIndex][previousActionIndex] = totalReward;
            if (reward < 0) {
                QLearning.debugPrint("Propagating blame to higher throttles");
                QLearning.debugPrint("Blamed " +  (previousActionIndex -1));
                for (int i = previousActionIndex + 1 ; i < qLearningData[previousStateIndex].length ; i++) {

                    qLearningData[previousStateIndex][i] = totalReward;                   
                }
//            }
            
        }
        
        QLearning.debugPrint("Future " + futureReward);
        QLearning.debugPrint("Val After " + qLearningData[previousStateIndex][previousActionIndex]);

    }
    
        //when training
    public int findNextActionIndex(int thisStIndex) {
        double explorationTemp = Math.max(1, getMaxExplore() - ((double)lifeTime) / (double)NUM_STEPS_BETWEEN_TEMP_UPDATES);
        QLearning.debugPrint("Temp :" + explorationTemp);
        
        if (lastLaps) {
            return findMax(thisStIndex);
        }
        
        double cumArr[] = new double[qLearningData[thisStIndex].length];
        cumArr[0] = Math.pow(Math.E, (double)qLearningData[thisStIndex][0] / explorationTemp);
        for (int i = 1; i < cumArr.length; i++) {
            cumArr[i] = cumArr[i - 1] + Math.pow(Math.E, (double)qLearningData[thisStIndex][i] / explorationTemp);
            QLearning.debugPrint("Cum array " + i + " Val: " + cumArr[i]);
        }
        Random r = new Random();
        double val = cumArr[cumArr.length - 1] * r.nextDouble();
       QLearning.debugPrint("Random val "  + val);

        int searched = Math.abs(Arrays.binarySearch(cumArr, val) + 1); 
        int index = Math.min(qLearningData[thisStIndex].length - 1, Math.max(0, searched));
        
        while (qLearningData[thisStIndex][index] < 0 && index > 0) {
            index --;
        }
        
        QLearning.debugPrint("Chosen action " + Math.abs(index));
        QLearning.debugPrint("Best action " + findMax(thisStIndex));
        
        return index;

    }

    //  A method that doesn't try to learn anything, just exploits knowledge. 
    public int exploitKnowledge(int thisStInd) {
        int maxind = findMax(thisStInd);
        if (qLearningData[thisStInd][maxind] < 0) {
            //In this case the best option is either going to lead to crash or we haven't
            //    learned anything, so no point in exploitingç. 
            QLearning.debugPrint("Not learned enough");
            return findNextActionIndex(thisStInd);
        }
        else
            return maxind;    
    }

    private double getMaxExplore() {
        return MAX_EXPLORE;

    }

    public int getLifeTime() {
       return lifeTime;
    }

    private int findMax(int thisStIndex) {

        QLearning.debugPrint("Maximisinng");
        int maxind = DEFAULT_THROTTLE;
        for (int i = 0; i < qLearningData[thisStIndex].length; i++) {
            QLearning.debugPrint("Val at " + i + " / " + qLearningData[thisStIndex][i]);
            if (qLearningData[thisStIndex][i] > qLearningData[thisStIndex][maxind]) {
                QLearning.debugPrint("New max");
                maxind = i;
            }
            if (lastLaps && qLearningData[thisStIndex][i] > 10E2) {
                maxind = i;
            }
        }
//        
//        if (getMaxExplore() < 2 && maxind +1  != qLearningData[thisStIndex].length && qLearningData[thisStIndex][maxind + 1] >= 0) {
//            QLearning.debugPrint("One higher");
//            maxind++;
//        }
        
        while (qLearningData[thisStIndex][maxind] < 0 && maxind > 0) {
            maxind --;
        }
         QLearning.debugPrint("");
        return maxind;
    }
    

    protected void printStatus() {
          printNumRaces();
          summariseQLearningMatrix();
    }
    
    
    
    private static void checkInjectivity(QLearnerBrains brains) {
        State s;
        double speed, angle, distToCurve, angleOfCurve, radiusOfCurve;
        distToCurve = 0;
        speed = brains.MINIMUM_SPEED - 0.001;
        angle = brains.MINIMUM_ANGLE - 0.001;
        angleOfCurve = 0;
        radiusOfCurve = 0;
        while (distToCurve < brains.BUCKETSIZE_DISTANCE_TO_CURVE * brains.NUM_DISTANCE_TO_CURVE_BUCKETS) {
            while (angleOfCurve < brains.BUCKETSIZE_ANGLE_OF_NEXT_CURVE * brains.NUM_ANGLE_OF_NEXT_CURVE_BUCKETS) {
                while (speed < brains.BUCKETSIZE_SPEED * brains.NUM_SPEED_BUCKETS + brains.MINIMUM_SPEED) {
                    while(angle < brains.BUCKETSIZE_ANGLE * brains.NUM_ANGLE_BUCKETS + brains.MINIMUM_ANGLE) {
                        while(radiusOfCurve < brains.BUCKETSIZE_RADIUS * brains.NUM_RADIUS_BUCKETS) {
                            s = new State(speed, angle, distToCurve, angleOfCurve, radiusOfCurve, 0, 0);
                            System.out.println(s);
                            System.out.println(" Index: " + brains.getStateIndex(s));
//                            System.out.println("");
                            if ( brains.getStateIndex(s) >= brains.getNumberOfStates() -1) {
                                System.out.println("TOO MANY: ");
                                return;
                            }
                            s = new State(speed, -1 * angle, distToCurve, angleOfCurve, radiusOfCurve, 0, 0);
//                            System.out.println(s);

                            System.out.println(" Index: " + brains.getStateIndex(s));
//                            System.out.println("");
                                  
                            if ( brains.getStateIndex(s) >= brains.getNumberOfStates() -1 ) {
                                System.out.println("TOO MANY: ");
                                return;
                            }
                            radiusOfCurve += brains.BUCKETSIZE_RADIUS;
                        }
                        angle += brains.BUCKETSIZE_ANGLE;
                        radiusOfCurve = 0;
                    }
                    speed += brains.BUCKETSIZE_SPEED;
                    angle = brains.MINIMUM_ANGLE - 0.001;
                }
                angleOfCurve += brains.BUCKETSIZE_ANGLE_OF_NEXT_CURVE;
                speed = brains.MINIMUM_SPEED - 0.001;
            }
            distToCurve += brains.BUCKETSIZE_DISTANCE_TO_CURVE;
            angleOfCurve = 0;
        }
    }
      
    public static void main(String[] args) {
        
        QLearnerBrains brains = new QLearnerBrains(30, 10, 0.25, 1.5, 10, 5, 11);
 
        
        //        double BUCKETSIZE_DISTANCE_TO_CURVE,
//        double BUCKETSIZE_ANGLE_OF_NEXT_CURVE,
//        double BUCKETSIZE_SPEED,
//        double BUCKETSIZE_ANGLE,
//        double BUCKETSIZE_RADIUS,
//        double BASE_FOR_REWARD,
//        int NUM_OF_ACTIONS)
        
////        System.out.println("Tot states " +  brains.getNumberOfStates());
        
//        checkInjectivity(brains);
//        testNextAngle(brains);
//        testNextSpeed(brains);
//        testPreviousDistToCurve(brains);
//        
        
    
    }



   

}
