/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai;

import curvecrawlers.Serialiser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;

public class LightBrains implements Serializable {

    private int[] actions;

    // Diskretisation of state;
    private final double BUCKETSIZE_DISTANCE_TO_CURVE;  //= 20;
    private final int NUM_DISTANCE_TO_CURVE_BUCKETS = 10;

    private final double BUCKETSIZE_ANGLE_OF_NEXT_CURVE;  //= 5;
    private final int NUM_ANGLE_OF_NEXT_CURVE_BUCKETS = 10;

    private final double BUCKETSIZE_SPEED; // = 0.5; // = 0.5;
    private final int NUM_SPEED_BUCKETS = 14;
    private final double MINIMUM_SPEED = 3;

    private final double BUCKETSIZE_ANGLE; // = 1;
    private final int NUM_ANGLE_BUCKETS = 30;
    private final double MINIMUM_ANGLE = 0;

    private final double BUCKETSIZE_RADIUS; // = 20;
    private final int NUM_RADIUS_BUCKETS = 10;

    private final int NUM_OF_DIFFERENT_SIGN_BUCKETS = 2;

    public LightBrains(double BUCKETSIZE_DISTANCE_TO_CURVE,
                       double BUCKETSIZE_ANGLE_OF_NEXT_CURVE,
                       double BUCKETSIZE_SPEED,
                       double BUCKETSIZE_ANGLE,
                       double BUCKETSIZE_RADIUS,
                       int[] states) {
        this.BUCKETSIZE_DISTANCE_TO_CURVE = BUCKETSIZE_DISTANCE_TO_CURVE;
        this.BUCKETSIZE_ANGLE_OF_NEXT_CURVE = BUCKETSIZE_ANGLE_OF_NEXT_CURVE;
        this.BUCKETSIZE_SPEED = BUCKETSIZE_SPEED;
        this.BUCKETSIZE_ANGLE = BUCKETSIZE_ANGLE;
        this.BUCKETSIZE_RADIUS = BUCKETSIZE_RADIUS;
        this.actions = states;
    }

    public int exploitKnowledge(int thisStInd) {
        return actions[Math.min(getNumberOfStates() - 1, Math.max(0, thisStInd))];
    }

    private int getNumberOfStates() {
        return NUM_DISTANCE_TO_CURVE_BUCKETS
                * NUM_ANGLE_OF_NEXT_CURVE_BUCKETS
                * NUM_SPEED_BUCKETS * NUM_ANGLE_BUCKETS
                * NUM_RADIUS_BUCKETS * NUM_OF_DIFFERENT_SIGN_BUCKETS;
    }

    public int getStateIndex(State s) {
        int distanceIndex = getCorrectIndex(s.getDistanceToNextCurve(), NUM_DISTANCE_TO_CURVE_BUCKETS, BUCKETSIZE_DISTANCE_TO_CURVE);
        int angleOfCurveIndex = getCorrectIndex(s.getAngleOfNextCurve(), NUM_ANGLE_OF_NEXT_CURVE_BUCKETS, BUCKETSIZE_ANGLE_OF_NEXT_CURVE);
        int speedIndex = s.getSpeed() < MINIMUM_SPEED ? 0 : getCorrectIndex(s.getSpeed() - MINIMUM_SPEED, NUM_SPEED_BUCKETS, BUCKETSIZE_SPEED);
        int angleIndex = s.getAngle() < MINIMUM_ANGLE ? 0 : getCorrectIndex(s.getAngle() - MINIMUM_ANGLE, NUM_ANGLE_BUCKETS, BUCKETSIZE_ANGLE);
        int radiusIndex = getCorrectIndex(s.getRadiusOfNextCurve(), NUM_RADIUS_BUCKETS, BUCKETSIZE_RADIUS);
        int signIndex = s.isDifferentAngleSign() ? 1 : 0;

        QLearning.debugPrint("Action table is null: " + (actions == null));
        
        return distanceIndex * NUM_ANGLE_OF_NEXT_CURVE_BUCKETS
                * NUM_SPEED_BUCKETS
                * NUM_ANGLE_BUCKETS
                * NUM_RADIUS_BUCKETS
                * NUM_OF_DIFFERENT_SIGN_BUCKETS
                + angleOfCurveIndex * NUM_SPEED_BUCKETS
                * NUM_ANGLE_BUCKETS
                * NUM_RADIUS_BUCKETS
                * NUM_OF_DIFFERENT_SIGN_BUCKETS
                + speedIndex * NUM_ANGLE_BUCKETS
                * NUM_RADIUS_BUCKETS
                * NUM_OF_DIFFERENT_SIGN_BUCKETS
                + angleIndex * NUM_RADIUS_BUCKETS
                * NUM_OF_DIFFERENT_SIGN_BUCKETS
                + radiusIndex * NUM_OF_DIFFERENT_SIGN_BUCKETS
                + signIndex;
    }

    private int getCorrectIndex(double val, int numBuckets, double sizeOfBucket) {
        // to fix rounding
        double toAdd = sizeOfBucket / 2;
        //assumes that casting to an int works like floor (i.e (int)(x.abcd... = x) 
        return Math.min(numBuckets - 1, Math.max(0, (int)((val + toAdd) / sizeOfBucket)));

    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        String qLearningFileName = "QLearnerA.dat";
        String lightBrainName = "src/main/resources/lBrain.dat";

        Serialiser<QLearnerBrains> brainS = new Serialiser<>();
        Serialiser<LightBrains> lBrainS = new Serialiser<>();

        QLearnerBrains oldBrain;
        LightBrains light;

        if (new File(qLearningFileName).isFile()) {
            System.out.println("Found old Brains");
            oldBrain = brainS.deserialize(qLearningFileName);
        }
        else {
            System.out.println("Error no brains found");
            return;
        }
        light = new LightBrains(30.0, 10.0, 0.5, 2.0, 10.0, oldBrain.lobotomize());
        lBrainS.serialize(light, lightBrainName);

    }

}
