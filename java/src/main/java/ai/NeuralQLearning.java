/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ai;

// Harjoitukset tehtävä Jeremias Berg

import java.io.IOException;
import java.util.Random;
import message.*;


public class NeuralQLearning extends AI {
    
    private final NeuralNetQLearning brain;
    
    private NeuralState lastState;
    private int lastAction;
    public static final double epsilon = 0.4;
    private boolean shouldUpdate;

    
    
    public NeuralQLearning(String host, int port, String botName, String botKey, NeuralNetQLearning brains) throws IOException {
        super(host, port, botName, botKey);
        this.brain = brains;
        lastState = null;
        shouldUpdate = true;
    }
    
    @Override
    public SendMessage getNextMessage() {
        NeuralState curState = getCurrentState();
        
        if (curState.getCarSpeed() < 0.5) {
            lastState = curState;
            lastAction = 10;
            return new Throttle(1.0);
        }
        
        brain.debugPrint("------NEW TICK------");
        brain.debugPrint("Cur state " + curState);
        
        if (shouldUpdate && curState != null && lastState != null) {
            addDataPoint(curState);
        }

        shouldUpdate = !isCrashed();
        if (!shouldUpdate) {
            lastState = curState;
            brain.debugPrint("Crashed");
            return new Throttle(1.0);
        }
        
        int actionIndex = getNextAction(curState);
        
        brain.debugPrint("Chosen action " + actionIndex);
        brain.debugPrint("Last action " + lastAction);

        lastState = curState;
        
        lastAction = actionIndex;

        brain.debugPrint("-----END OF TICK------ \n");
        return convertIndexToMessage(actionIndex);
    }

    private int getNextAction(NeuralState curState) {
        Random r = new Random();
        if (r.nextDouble() < epsilon && NeuralNetQLearning.training) {
            System.out.println("Random action");
            return randomAction();
        }      
        int actionIndex = 5;
        double maxVal = brain.runNetwork(curState, actionIndex);
        double val;
        for (int i = 0 ; i < NeuralNetQLearning.NUM_ACTIONS ; i++) {
            val = brain.runNetwork(curState, i);
            NeuralNetQLearning.debugPrint("Testing action "+ i + " Got val " + val);
            if (val > maxVal) {
                NeuralNetQLearning.debugPrint("Improved");
                actionIndex = i;
                maxVal = val;
            }
        }
        return actionIndex;
    }

    private void addDataPoint(NeuralState s) {
        if (lastState == null || s == null) {
            return;
        }
            
        
        double revard;
        if (isCrashed()) {
            revard = -20;
        }
        else 
            revard = s.getCarSpeed();
        
        LearningPattern toadd = new LearningPattern(lastState, lastAction, revard, s);
        NeuralNetQLearning.debugPrint("Adding data point " + toadd);
        
        brain.addLearningPattern(toadd);
        
    }

    @Override
    protected void afterRace() {
        brain.checkTraining();
    }

    private SendMessage convertIndexToMessage(int actionIndex) {
        return new Throttle(((double)actionIndex) / (10.0));

            
            
    }

    private int randomAction() {
        Random r = new Random();
        
        return  r.nextInt(NeuralNetQLearning.NUM_ACTIONS);
              
    }
}
