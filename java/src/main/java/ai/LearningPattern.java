/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ai;

// Harjoitukset tehtävä Jeremias Berg

import java.util.Objects;

public class LearningPattern {
    
    private final NeuralState prevState;
    private final int chosenAction;
    private final double reward;
    private final NeuralState curState;

    public LearningPattern(NeuralState prevState, 
                           int chosenAction,
                           double reward, 
                           NeuralState curState) {
        this.prevState = prevState;
        this.chosenAction = chosenAction;
        this.reward = reward;
        this.curState = curState;
    }
    
    public NeuralState getPrevState() {
        return prevState;
    }

    public int getChosenAction() {
        return chosenAction;
    }

    public double getReward() {
        return reward;
    }

    public NeuralState getCurState() {
        return curState;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.prevState);
        hash = 23 * hash + this.chosenAction;
        hash = 23 * hash + (int)(Double.doubleToLongBits(this.reward) ^ (Double.doubleToLongBits(this.reward) >>> 32));
        hash = 23 * hash + Objects.hashCode(this.curState);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LearningPattern other = (LearningPattern)obj;
        if (!this.prevState.equals(other.prevState)) {
            return false;
        }
        if (this.chosenAction != other.chosenAction) {
            return false;
        }
        if (Double.doubleToLongBits(this.reward) != Double.doubleToLongBits(other.reward)) {
            return false;
        }
        return !this.curState.equals(other.curState);
    }
    
    @Override
    public String toString() {
        return "Prev state \n" + prevState + " Cur State \n" + curState + " Action " + chosenAction + " Reward " + reward; 
    }
    
    
    
    
    
    
    
    
}
