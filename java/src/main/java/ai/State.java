/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai;

public class State {

    private final double speed;
    private final double angle;
    private final double distanceToNextCurve;
    private final double angleOfNextCurve;
    private final double radiusOfNextCurve;
    private final boolean differentAngleSign;
    private final int curLap;
    private final double curAngle; 

    public State(double speed, double angle, 
                 double distanceToNextCurve, double angleOfNextCurve,
                 double radiusofNextCurve, int curLap, double curAngl) {
        this.speed = Math.max(speed, 0);
        this.angle = Math.abs(curAngl - angle);
        this.distanceToNextCurve =  Math.max(0, distanceToNextCurve);
        this.angleOfNextCurve = Math.abs(angleOfNextCurve);
        this.radiusOfNextCurve = Math.max(radiusofNextCurve, 0);
        this.differentAngleSign = (angle < 0 && curAngl > 0) || (angle > 0 && curAngl < 0);
        this.curLap = curLap;
        this.curAngle = curAngl;
    }
    
    public State (double speed, double angle, 
                   double distanceToNextCurve, double angleOfNextCurve,
                   double radiusofNextCurve, int curLap, double curAngl, double angleAdd) {
        this.speed = Math.max(speed, 0);
        this.angle = angle + angleAdd;
        this.distanceToNextCurve =  Math.max(0, distanceToNextCurve);
        this.angleOfNextCurve = Math.abs(angleOfNextCurve);
        this.radiusOfNextCurve = Math.max(radiusofNextCurve, 0);
        this.differentAngleSign = (angle < 0 && angleOfNextCurve > 0) || (angle > 0 && angleOfNextCurve < 0);
        this.curLap = curLap;
        this.curAngle = curAngl;
    }

    public boolean isDifferentAngleSign() {
        return differentAngleSign;
    }

    public double getSpeed() {
        return speed;
    }

    public double getAngle() {
        return angle;
    }

    public double getDistanceToNextCurve() {
        return distanceToNextCurve;
    }

    public double getAngleOfNextCurve() {
        return angleOfNextCurve;
    }

    public double getRadiusOfNextCurve() {
        return radiusOfNextCurve;
    }
    
    @Override
    public String toString() {
        return ("Current State\n"
                + "Speed: " + speed + " \n "
                + "Angle of car " + angle+ " \n "
                + "Distance to Curve " + distanceToNextCurve + " \n "
                + "Angle of Next Curve " + angleOfNextCurve +  " \n "
                + "Radius of Next Curve " + radiusOfNextCurve)+  " \n "
                + "Is different signs: " + isDifferentAngleSign();
    }

    public int getLap() {

       return curLap;    
    }

    double getCurAngl() {
        return curAngle;    
    }


    
    

}
