/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai;

import java.io.IOException;
import message.*;

public class QLearning extends AI {

    private static final boolean debug = false;
    private final LightBrains brains;

    public QLearning(String host, int port, String botName, String botKey, LightBrains brains) throws IOException {
        super(host, port, botName, botKey);

        this.brains = brains;

//        brains.printStatus();
    }

    @Override
    public SendMessage getNextMessage() {
        debugPrint("------------NEWTICK-----------------------------");
        // State curState = getCurrentState();

        int thisStIndex =  0; //brains.getStateIndex(curState);

        if (isCrashed()) {
            return new Throttle(1.0); // No point of learning anything about being crashed.
        }
        int newActionIndex = brains.exploitKnowledge(thisStIndex);

        debugPrint("Cur state " + thisStIndex);
        debugPrint("Cur action " + newActionIndex);

        SendMessage toSend = convertIndexToMessage(newActionIndex);
        debugPrint("------------ENDOFTICK-----------------------------");
        return toSend;
    }

    /*
     Method for extracting whatever is required to serialize for continued learning
     */
    public LightBrains getBrains() {
        return this.brains;
    }

    /*
     Actions are parametrizised according to following:
     Index: 0 => Ping()
     Indexes: 1 -> NUM_ACTIONS -> throttle index - 1 / 10
     */
    private SendMessage convertIndexToMessage(int newActionIndex) {
        if (newActionIndex == 0) {
            return new Ping();
        }
        return new Throttle((double)(newActionIndex) / (double)10);
    }

    public static void debugPrint(String string) {
        if (debug) {
            System.out.println(string);
        }
    }

    @Override
    protected void afterRace() { return; }

}
