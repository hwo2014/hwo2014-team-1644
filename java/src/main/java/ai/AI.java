package ai;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import curvecrawlers.Timer;
import dataholder.carpositions.CarPosition;
import dataholder.carpositions.Id;
import dataholder.carpositions.PiecePosition;
import dataholder.gameinit.Piece;
import static dataholder.gameinit.Piece.PieceType.*;
import dataholder.gameinit.Race;
import dataholder.gameinit.Track;
import dataholder.parsers.CarPositionsParser;
import dataholder.parsers.GameInitParser;
import dataholder.parsers.IdParser;
import dataholder.parsers.TurboParser;
import dataholder.turbo.Turbo;
import helper.Printer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import message.CreateRace;
import message.Join;
import message.JoinRace;
import message.Ping;
import message.ReceiveMessage;
import message.SendMessage;
import message.Switch;
import static message.Switch.Direction.*;
import message.TurboMessage;

/**
 *
 * @author Lasse Lybeck
 */
public abstract class AI {

    private final JsonParser jsonParser;
    private final PrintWriter writer;
    private final BufferedReader reader;
    private final String botName;
    private final String botKey;
    private Track track;
    private Id myCar;
    private CarPosition myPosition, lastPosition;
    private double lastThrottle;
    private boolean crashed;
    private NeuralState currentState;
    private int lastLaneIndex;
    private boolean switching;
    private Turbo turbo;
    
    private boolean turboAv;
    private boolean inTurbo;
    private int gameTick;
    private final Timer timer;
    
    private static final boolean USE_TURBO = false;

    public AI(String host, int port, String botName, String botKey) throws IOException {
        Socket socket = new Socket(host, port);
        this.writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        this.jsonParser = new JsonParser();
        this.botName = botName;
        this.botKey = botKey;
        this.crashed = false;
        this.timer = new Timer();
    }

    /**
     * Starts the traditional "Keimola" track without any extra specifications
     *
     * @throws IOException
     */
    public void race() throws IOException {
        joinGame();
        runRace();
    }

    public void race(String trackName, String password, int carCount) throws IOException {
//        System.out.println("Starting a race on: " + trackName);
        createRace(trackName, password, carCount);
        runRace();
    }

    public void race(String trackName, String password, int carCount, boolean join) throws IOException {
//        System.out.println("Trying to join: " + trackName);
        if (join) {
            joinRace(trackName, password, carCount);
        } else {
            createRace(trackName, password, carCount);
        }
        runRace();
    }

    private ReceiveMessage parseMessage(String jsonString) {
        ReceiveMessage receiveMessage = new ReceiveMessage();
        JsonObject jsonObject = jsonParser.parse(jsonString).getAsJsonObject();
        receiveMessage.setMessageType(jsonObject.get("msgType").getAsString());
        receiveMessage.setData(jsonObject.get("data"));
        JsonElement tickElement = jsonObject.get("gameTick");
        if (tickElement != null) {
            receiveMessage.setGameTick(tickElement.getAsInt());
        }
        return receiveMessage;
    }

    private void send(final SendMessage msg) {
//        System.out.println("Sending: " + msg.toJson());
        writer.println(msg.toJson());
        writer.flush();
    }

    private CarPosition getMyPosition(List<CarPosition> carPositions) {
        for (CarPosition carPosition : carPositions) {
            if (carPosition.getId().equals(myCar)) {
                return carPosition;
            }
        }
        // this will never happen!
        return null;
    }

    private void runRace() throws IOException {

        String jsonMessage;
        turboAv = false;
        inTurbo = false;
        while ((jsonMessage = reader.readLine()) != null) {
            timer.tic();
            ReceiveMessage serverMessage = parseMessage(jsonMessage);
            String messageType = serverMessage.getMessageType();
            
            
            switch (messageType) {
                case "crash":
                    if (serverMessage.getGameTick() != null) {
                        this.gameTick = serverMessage.getGameTick();
                    }   // did we crash?
                    if (!myCar.equals(IdParser.parseId(serverMessage.getData()))) {
//                        System.out.println("Someone else crashed.");
                        break;
                    }
                    System.out.println("CRASH!");
                    crashed = true;
                    inTurbo = false;
                    break;
                case "spawn":
                    if (serverMessage.getGameTick() != null) {
                        this.gameTick = serverMessage.getGameTick();
                    }   // did we spawn?
                    if (!myCar.equals(IdParser.parseId(serverMessage.getData()))) {
//                        System.out.println("Someone else spawned.");
                        break;
                    }
//                    System.out.println("Spawned!");
                    crashed = false;
                    Curve nextCurve = getNextCurve(lastPosition);
                    
                    double curAngle = track.getPieces().get(lastPosition.getPiecePosition().getPieceIndex()).getAngle();
                    double curRadius = track.getPieces().get(lastPosition.getPiecePosition().getPieceIndex()).getRadius();

                    currentState = new NeuralState(0.0, 0.0, nextCurve.distanceTo, nextCurve.angle, nextCurve.radius, curAngle, curRadius, turboAv, inTurbo);
                    SendMessage nextMessage = getNextMessage();
                    send(nextMessage);
                    break;
            }

            switch (messageType) {
                case "carPositions":
                    if (serverMessage.getGameTick() != null) {
                        this.gameTick = serverMessage.getGameTick();
                    }
                    List<CarPosition> carPositions = CarPositionsParser.parseCarPositions(serverMessage.getData());
                    myPosition = getMyPosition(carPositions);
                    double speed = calculateSpeed(lastPosition, myPosition);
                    double angle = myPosition.getAngle();
                    Curve nextCurve = getNextCurve(myPosition);
                    System.out.println("Lap " + myPosition.getPiecePosition().getLap());
                    System.out.println("On piece= " + (myPosition.getPiecePosition().getPieceIndex() + 1) + "/" + track.getPieces().size());
                    if (checkTurbo(myPosition, nextCurve, speed))
                        break;
                    if (checkSwitch(myPosition))
                        break;
                    double curAngle = track.getPieces().get(myPosition.getPiecePosition().getPieceIndex()).getAngle();
                    double curRadius = track.getPieces().get(myPosition.getPiecePosition().getPieceIndex()).getRadius();
                    currentState = new NeuralState(speed, angle, nextCurve.distanceTo, nextCurve.angle, nextCurve.radius, curAngle, curRadius, turboAv, inTurbo);
                    SendMessage nextMessage = getNextMessage();
                    send(nextMessage);
                    lastPosition = myPosition;
                    break;
                case "turboAvailable":
                    this.turbo = TurboParser.parseTurbo(serverMessage.getData());
                    turboAv = true;
                    send(new Ping());
//                    Printer.printAsJson("Turbo available", turbo);
                    break;
                case "turboEnd":
                    if (myCar.equals(IdParser.parseId(serverMessage.getData()))) {
//                        System.out.println("Turbo end. Piece index: " + lastPosition.getPiecePosition().getPieceIndex());
                    } else {
//                        Printer.printAsJson(messageType, serverMessage.getData());
                    }
                    inTurbo = false;
                    
                    send(new Ping());
                    break;
                case "turboStart":
                    if (myCar.equals(IdParser.parseId(serverMessage.getData()))) {
//                        System.out.println("Turbo start. Piece index: " + lastPosition.getPiecePosition().getPieceIndex());
                    } else {
//                        Printer.printAsJson(messageType, serverMessage.getData());
                    }
                    

                    inTurbo = true;
                    turboAv = false;
                    send(new Ping()); 
                    break;
                case "join":
//                    System.out.println("Joined!");
                    send(new Ping());
                    break;
                case "yourCar":
                    this.myCar = IdParser.parseId(serverMessage.getData());
//                Printer.printAsJson("myCar", myCar);
                    send(new Ping());
                    break;
                case "gameInit":
                    System.out.println("Game init");
                    Race race = GameInitParser.parseGameInit(serverMessage.getData());
                    this.track = race.getTrack();
                    lastPosition = new CarPosition();
                    lastPosition.setPiecePosition(new PiecePosition());
//                    Printer.printAsJson("Track", race.getTrack());
//                    Printer.printAsJson("gameInit", race);
//                    Printer.printAsJson("merged pieces", track.getMergedPieceMap());
                    send(new Ping());
                    break;
                case "gameEnd":
                    System.out.println("Race end");
                    turbo = null;
                    switching = false;
                    send(new Ping());
                    break;
                case "gameStart":
                    System.out.println("Race start");
                    lastPosition = new CarPosition();
                    lastPosition.setPiecePosition(new PiecePosition());
                    send(new Ping());
                    break;
                case "error":
//                    System.err.println("ERROR!");
//                    System.err.println(serverMessage.getData());
                    send(new Ping());
                    break;
                default:
//                    Printer.printAsJson(messageType, serverMessage.getData());
                    send(new Ping());
                    break;
            }
            timer.toc();
            
        }   
//        afterRace();
    }
    
    protected boolean isStraight() {
        return track.getPieces().get(myPosition.getPiecePosition().getPieceIndex()).getType() == STRAIGHT;
    }

    private void joinGame() {
//        System.out.println("Sending join request...");
        send(new Join(botName, botKey));
    }

    private void createRace(String trackName, String password, int carCount) {
//        System.out.println("Sending createRace request...");
        send(new CreateRace(botName, botKey, trackName, password, carCount));
    }

    private void joinRace(String trackName, String password, int carCount) {
//        System.out.println("Sending joinRace request...");
        send(new JoinRace(botName, botKey, trackName, password, carCount));
    }

    private double calculateSpeed(CarPosition p1, CarPosition p2) {
        int i1 = p1.getPiecePosition().getPieceIndex();
        int i2 = p2.getPiecePosition().getPieceIndex();
        double dist = p2.getPiecePosition().getInPieceDistance() - p1.getPiecePosition().getInPieceDistance();
        if (i1 <= i2) {
            for (int i = i1; i < i2; i++) {
                dist += getActualPieceLength(i, p2);
            }
        } else {
            for (int i = i1; i < track.getPieces().size(); i++) {
                dist += getActualPieceLength(i, p2);
            }
            for (int i = 0; i < i2; i++) {
                dist += getActualPieceLength(i, p2);
            }
        }
        return dist / 1.0;
    }

    private double getActualPieceLength(int pieceIndex, CarPosition pos) {
        return getActualPieceLength(track.getPieces().get(pieceIndex), pos);
    }

    private double getActualPieceLength(Piece p, CarPosition pos) {
        return getActualPieceLength(p, pos.getPiecePosition().getLane().getEndLaneIndex());
    }

    private double getActualPieceLength(int pieceIndex, int laneIndex) {
        return getActualPieceLength(track.getPieces().get(pieceIndex), laneIndex);
    }

    private double getActualPieceLength(Piece p, int laneIndex) {
        if (p.getType() == Piece.PieceType.STRAIGHT) {
            return p.getLength();
        } else {
            return getActualCurveLength(p, laneIndex);
        }
    }

    private double getActualCurveLength(Piece piece, int laneIndex) {
        return piece.getLength() * getActualCurveRadius(piece, laneIndex) / piece.getRadius();
    }

    private int getActualCurveRadius(Piece piece, CarPosition pos) {
        return getActualCurveRadius(piece, pos.getPiecePosition().getLane().getStartLaneIndex());
    }

    private int getActualCurveRadius(Piece piece, int laneIndex) {
        return piece.getRadius() - ((int) Math.signum(piece.getAngle())) * track.getLanes().get(laneIndex).getDistanceFromCenter();
    }

    private Curve getNextCurve(CarPosition myPosition) {
        int myInd = myPosition.getPiecePosition().getPieceIndex();
        int radius = track.getPieces().get(myInd).getRadius();
        double angle = track.getPieces().get(myInd).getAngle();
        Piece.PieceType type = track.getPieces().get(myInd).getType();
        int sz = track.getPieces().size();
        Curve curve = new Curve();
        curve.distanceTo = getActualPieceLength(myInd, myPosition) - myPosition.getPiecePosition().getInPieceDistance();
        for (int i = (myInd + 1 < sz ? myInd + 1 : 0); i != myInd; i = (i + 1 < sz ? i + 1 : 0)) {
            Piece piece = track.getPieces().get(i);
            if (piece.getType() != type || piece.getRadius() != radius || piece.getAngle() * angle < 0) {
                curve.radius = getActualCurveRadius(piece, myPosition);
                for (int j = i; true; j = (j + 1 < sz ? j + 1 : 0)) {
                    Piece nextPiece = track.getPieces().get(j);
                    if (nextPiece.getType() != piece.getType() || getActualCurveRadius(nextPiece, myPosition) != curve.radius || nextPiece.getAngle() * curve.angle < 0) {
                        break;
                    }
                    curve.angle += track.getPieces().get(j).getAngle();
                }
                return curve;
            } else {
                curve.distanceTo += getActualPieceLength(i, myPosition);
            }
        }
        // never here...
        return null;
    }

    public Track getTrack() {
        return track;
    }

    public double getLastThrottle() {
        return lastThrottle;
    }

    public boolean isCrashed() {
        return crashed;
    }

    public NeuralState getCurrentState() {
        return currentState;
    }

    public CarPosition getMyPosition() {
        return myPosition;
    }

    public CarPosition getLastPosition() {
        return lastPosition;
    }

    public abstract SendMessage getNextMessage();

    private boolean checkSwitch(CarPosition myPosition) {
        int thisLaneIndex = myPosition.getPiecePosition().getLane().getEndLaneIndex();
        if (thisLaneIndex != lastLaneIndex) {
            switching = false;
        }
        List<Piece> pieces = track.getPieces();
        int thisPieceIndex = myPosition.getPiecePosition().getPieceIndex();
        int nextPieceIndex = thisPieceIndex + 1 < pieces.size() ? thisPieceIndex + 1 : 0;
        Piece nextPiece = pieces.get(nextPieceIndex);
        if (switching || !nextPiece.isSwitch()) {
            return false;
        }
        double thisLaneDist = getActualLaneDistanceToNextSwitch(nextPieceIndex, thisLaneIndex);
        int bestRelativeLane = 0;
        double bestDistance = thisLaneDist;
        if (thisLaneIndex + 1 < getTrack().getLanes().size()) {
            double distance = getActualLaneDistanceToNextSwitch(nextPieceIndex, thisLaneIndex + 1);
            if (distance < bestDistance) {
                bestDistance = distance;
                bestRelativeLane = 1;
            }
        }
        if (thisLaneIndex > 0) {
            double distance = getActualLaneDistanceToNextSwitch(nextPieceIndex, thisLaneIndex - 1);
            if (distance < bestDistance) {
                bestDistance = distance;
                bestRelativeLane = -1;
            }
        }
        if (bestRelativeLane == 1) {
            send(new Switch(RIGHT));
            switching = true;
        } else if (bestRelativeLane == -1) {
            send(new Switch(LEFT));
            switching = true;
        }
        
        lastLaneIndex = thisLaneIndex;
        
        return switching;
    }

    private double getActualLaneDistanceToNextSwitch(int pieceIndex, int laneIndex) {
        int sz = track.getPieces().size();
        List<Piece> pieces = track.getPieces();
        double dist = 0;
        for (int i = (pieceIndex + 1 < sz ? pieceIndex + 1 : 0); i != pieceIndex; i = (i + 1 < sz ? i + 1 : 0)) {
            if (pieces.get(i).isSwitch()) {
                break;
            }
            dist += getActualPieceLength(i, laneIndex);
        }
        return dist;
    }

    protected boolean isTurboAvailable() {
        return getTurbo() != null;
    }

    protected Turbo getTurbo() {
        return turbo;
    }

    private boolean checkTurbo(CarPosition myPosition, Curve nextCurve, double speed) {
        if (!USE_TURBO)
            return false;
        if (!isTurboAvailable()) {
            return false;
        }
        Turbo t = getTurbo();
        double maxSpeedEst = t.getTurboFactor() * 10 * TURBO_SPEED_EST_FACTOR;
        double tDist = maxSpeedEst * t.getTurboDurationTicks();

//        System.out.println("---");
//        System.out.println("Turbo available!!");
//        System.out.println("maxSpeedEst = " + maxSpeedEst);
//        System.out.println("tDist = " + tDist);
//        Printer.printAsJson("Next curve", nextCurve);
//        System.out.println("---");
        Piece.PieceType thisPieceType = track.getPieces().get(myPosition.getPiecePosition().getPieceIndex()).getType();
        if (thisPieceType == STRAIGHT && tDist < nextCurve.distanceTo) {
//            System.out.println("TURBO! Piece index: " + myPosition.getPiecePosition().getPieceIndex());
//            System.out.println("tDist = " + tDist + ", nextCurve.distanceTo = " + nextCurve.distanceTo);
            send(new TurboMessage());
            turbo = null;
            return true;
        }
        return false;
    }
    private static final double TURBO_SPEED_EST_FACTOR = .7;

    protected abstract void afterRace();

    private static class Curve {

        private double distanceTo;
        private double angle;
        private double radius;
    }
}
