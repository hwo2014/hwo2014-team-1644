package ai;

import dataholder.carpositions.CarPosition;
import dataholder.gameinit.Piece;
import java.io.IOException;
import java.util.List;
import message.SendMessage;
import message.Throttle;

/**
 *
 * @author Lasse Lybeck
 */
public class NoobAI extends AI {

    private double lastThrottle;

    public NoobAI(String host, int port, String botName, String botKey) throws IOException {
        super(host, port, botName, botKey);
    }

    private double getNextThrottle() {
        CarPosition myPosition = getMyPosition();
        int pieceIndex = myPosition.getPiecePosition().getPieceIndex();
        double speed = getCurrentState().getCarSpeed();
        List<Piece> pieces = getTrack().getPieces();
        Piece thisPiece = pieces.get(pieceIndex);
        Piece nextPiece = pieces.get(pieceIndex + 1 < pieces.size() ? pieceIndex + 1 : 0);
        Piece afterNextPiece = pieces.get(pieceIndex + 2 < pieces.size() ? pieceIndex + 2 : 0);
        double throttle = .65;
        if (thisPiece.getType() == Piece.PieceType.STRAIGHT) {
            if (nextPiece.getType() == Piece.PieceType.STRAIGHT) {
                if (afterNextPiece.getType() == Piece.PieceType.CURVE && afterNextPiece.getRadius() < 150 && speed > 5.0) {
                    throttle = .6;
                } else {
                    throttle = 1.0;
                }
            } else if (nextPiece.getRadius() < 150) {
                if (speed > 6.5) {
                    throttle = .25;
                } else {
                    throttle = .8;
                }
            } else {
                throttle = 1.0;
            }
        } else if (Math.abs(myPosition.getAngle()) < 15.0) {
            throttle = Math.max(1.3 * getLastThrottle(), .6);
        } else {
            throttle = Math.max(0.5 * getLastThrottle(), .35);
        }
        throttle = Math.min(1, throttle);
        lastThrottle = throttle;
        return throttle;
    }

    @Override
    public SendMessage getNextMessage() {
        return new Throttle(getNextThrottle());
    }

    @Override
    protected void afterRace() { return;    }
}
