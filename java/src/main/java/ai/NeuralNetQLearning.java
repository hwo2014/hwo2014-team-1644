/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai;

import static ai.NeuralQLearning.epsilon;
import curvecrawlers.RaceMain;
import java.util.LinkedList;
import java.util.Random;

public class NeuralNetQLearning {

    public static boolean debug = false;
    
    public static final int NUM_INPUT_NEURONS = 10;
    
    public static final boolean training = false; 
    public static final int NUM_ACTIONS = 11;
    private static final int NUM_HIDDEN_NEURONS = 7;
    private static final double MAX_WEIGHT_CHANGE = 10;
    private static final double MIN_WEIGHT_CHANGE = 10E-5;

    private static final double FACTOR_FOR_FUTURE = 0.2;

    private static final double etaMinus = 0.5;
    private static final double etaPlus = 1.2;

    private static final double biasOutput = 1.0;

    private static final int NUM_SAMPLES_BEFORE_TRAIN = 5000;
    private static final int ITERATIONS_PER_TRAINING = 20;
    
    // All weights definied as the weights in to a node
    private double[][] hiddenLayerWeights;
    private double[] outputLayerWeights;

    // Error gradients (only need the sign so store as int)
    private double[][] hiddenLayerGradients;
    private double[] outputGradients;
    private double outputDelta;

    // Weight changes (delta_ij)
    private double[][] hiddenLayerChange;
    private double[] outputLayerchange;

    // Weight last changes
    private double[][] hiddenLayerPrevChange;
    private double[] outputLayerPrevchange;

    private double[] hiddenLayerOutput;
    private double[] inputLayerOutput;

    private LinkedList<LearningPattern> data;
    private int lifeTime;

    public NeuralNetQLearning() {
        initializeFields();
        initializeWeights();
        initializeWeightChanges();
        zeroGradients();
        initializePrevChanges();
        data = new LinkedList<>();
        lifeTime = 0;
        summarizeNetwork();
    }
    
    public NeuralNetQLearning(NeuralNetworkInterns i) {
        initializeFields();
        this.outputLayerWeights = i.getOutputLayerWeights();
        this.hiddenLayerWeights = i.getHiddenLayerWeights();
        
        this.hiddenLayerChange = i.getHiddenLayerChange();
        this.outputLayerchange = i.getOutputLayerchange();
        
        this.hiddenLayerGradients = i.getHiddenLayerGradients();
        this.outputGradients = i.getOutputGradients();
        
        this.hiddenLayerPrevChange = i.getHiddenLayerPrevChange();
        this.outputLayerchange = i.getOutputLayerPrevchange();
        
        this.lifeTime = i.getLifeTime();
        
        data = new LinkedList<>();
        
        summarizeNetwork();
    }

    public double runNetwork(NeuralState s, int action) {
        double[] inp = s.summarize();
        if (inp.length != NUM_INPUT_NEURONS) {
            throw new IllegalArgumentException("Wrong amount of inputs to neural network");
        }
        int rAct = action;
        if (action < 0 || action > NUM_ACTIONS - 1) {
            System.out.println("WARNING, tried to run network with action " + action);
            rAct = Math.max(0, Math.min(NUM_ACTIONS - 1, action));
        }

        inp[NUM_INPUT_NEURONS - 1] = rAct;
        return runNetwork(inp);
    }
    
    public void checkTraining() {

        
        debugPrint("Amount of Data " + data.size());
        if (data.size() < NUM_SAMPLES_BEFORE_TRAIN) {
            debugPrint("Not enough data for training");
            return;
        }
        
        System.out.println("Before training");
        summarizeNetwork();
        
        trainNetwork();
        
        System.out.println("After training");
        summarizeNetwork();
        
        LinkedList<LearningPattern> old = data; 
        data = new LinkedList<>();
        Random r = new Random();
        
        for (LearningPattern e : old) {
            if (r.nextDouble() > 0.75 && e.getCurState() != null && e.getPrevState() != null) {
                data.push(e);
            }           
        }
        debugPrint("After training data " + data.size());
        
    }
    
    public void addLifeTime() {
        lifeTime++;
    }
    
    public int getLifeTime() {
        return lifeTime;
    }
    
    public NeuralNetworkInterns getWeights() {
        forceTraining();
        return new NeuralNetworkInterns(hiddenLayerWeights, outputLayerWeights, hiddenLayerGradients, outputGradients, hiddenLayerChange, outputLayerchange, hiddenLayerPrevChange, outputLayerPrevchange, lifeTime);
    }
    
    private void forceTraining() {
        trainNetwork();
        data = new LinkedList<>();
    }
    
    public final void summarizeNetwork() {
        System.out.println("Life Time " + getLifeTime());

        System.out.println("Weight hidden layer: ");
        for (int i = 0 ; i < hiddenLayerWeights.length ; i++) {
            System.out.print("Hidden neuron " + i +"\nWeights:");
            for (int j = 0 ; j < hiddenLayerWeights[i].length ; j++) {
                System.out.print(" " + hiddenLayerWeights[i][j]);               
            }
            System.out.println("\n");           
        }
        System.out.println("Output weights:");
        for (int i = 0 ; i < outputLayerWeights.length ; i++) {
            System.out.print(" " + outputLayerWeights[i]);
        }
        System.out.println("\n");
    }
    
    public void addLearningPattern(LearningPattern e) {
        if (training)
            data.push(e);
    }
   
    private void initializeWeightChanges() {
        double initialChange = 0.1;

        for (int i = 0 ; i < outputLayerchange.length ; i++) {
            outputLayerchange[i] = initialChange;

        }
        for (double[] hLayerChange : hiddenLayerChange) {
            for (int j = 0 ; j < hLayerChange.length ; j++) {
                hLayerChange[j] = initialChange;
            }

        }
    }

    private void initializeFields() {

        this.outputGradients = new double[NUM_HIDDEN_NEURONS + 1];
        this.hiddenLayerGradients = new double[NUM_HIDDEN_NEURONS][NUM_INPUT_NEURONS + 1];

        this.hiddenLayerChange = new double[NUM_HIDDEN_NEURONS][NUM_INPUT_NEURONS +1];
        this.outputLayerchange = new double[NUM_HIDDEN_NEURONS + 1];

        this.hiddenLayerPrevChange = new double[NUM_HIDDEN_NEURONS][NUM_INPUT_NEURONS +1];
        this.outputLayerPrevchange = new double[NUM_HIDDEN_NEURONS + 1];

        this.hiddenLayerOutput = new double[NUM_HIDDEN_NEURONS];
    }

    private void zeroGradients() {
        for (int i = 0 ; i < outputGradients.length ; i++) {
            outputGradients[i] = 0;
        }
        for (double[] hiddenLayerG : hiddenLayerGradients) {
            for (int i = 0 ; i < hiddenLayerG.length ; i++) {
                hiddenLayerG[i] = 0;
            }
        }
    }

    private void initializePrevChanges() {

        for (int i = 0 ; i < outputLayerPrevchange.length ; i++) {
            outputLayerPrevchange[i] = 0;

        }
        for (double[] hLayerChange : hiddenLayerPrevChange) {
            for (int j = 0 ; j < hLayerChange.length ; j++) {
                hLayerChange[j] = 0;
            }

        }
    }


    private void initializeWeights() {
        Random r = new Random();
        double correctionTerm = 0.5;

        this.hiddenLayerWeights = new double[NUM_HIDDEN_NEURONS][NUM_INPUT_NEURONS + 1];
        // 1 output node
        this.outputLayerWeights = new double[NUM_HIDDEN_NEURONS + 1];
        
        for (int i = 0 ; i < outputLayerWeights.length ; i++) {
            outputLayerWeights[i] = r.nextDouble() - correctionTerm;

        }
        for (double[] hWeights : hiddenLayerWeights) {
            for (int j = 0 ; j < hWeights.length ; j++) {
                hWeights[j] = r.nextDouble() - correctionTerm;
            }

        }
    }

    private double runNetwork(double[] input) {
        inputLayerOutput = new double[input.length]; 

        for (int i = 0 ; i < input.length ; i++) {
            inputLayerOutput[i] = sigmoid(input[i]);
            
        }
        
         debugPrint("The input is");
         debugPrint(neatlyPrint(input));
        
        
        runHiddenLayer();
        
        double qval = calculateOutput();
        return qval;
    }

    private double sigmoid(double input) {
        return 1.0 / (1 + Math.pow(Math.E, (-1) * input));
    }

    private void runHiddenLayer() {
        double temp;
        
//        debugPrint("Running Hidden layer");
        
        for (int i = 0 ; i < hiddenLayerOutput.length ; i++) {
//            debugPrint("At neuron: " + i);
            temp = 0;
            for (int j = 0 ; j < hiddenLayerWeights[i].length - 1 ; j++) {
//                debugPrint("Input output: " + inputLayerOutput[j] + " weight: "  + hiddenLayerWeights[i][j]);
                temp += (inputLayerOutput[j] * hiddenLayerWeights[i][j]);
            }
//            debugPrint("Bias output: " + biasOutput + " weight: "  + hiddenLayerWeights[i][hiddenLayerWeights[i].length - 1]);
            temp += (biasOutput * hiddenLayerWeights[i][hiddenLayerWeights[i].length - 1]);
//            debugPrint("Temp val: " + temp + " sigmoid: " + sigmoid(temp));
            hiddenLayerOutput[i] = sigmoid(temp);
        }
    }

    private double calculateOutput() {
        double o = 0;
//        debugPrint("Running outputLayer");
        for (int i = 0 ; i < outputLayerWeights.length ; i++) {
            if (i != outputLayerWeights.length - 1) {
//               debugPrint("hidden output: " + hiddenLayerOutput[i] + " weight: "  + outputLayerWeights[i]);
                o += hiddenLayerOutput[i] * outputLayerWeights[i];
            }
            else {
//                 debugPrint("Bias output: " + biasOutput + " weight: "  + outputLayerWeights[i]);
                 o += biasOutput * outputLayerWeights[i];
            }

        }
//        debugPrint("Output " + o);
        return o;
    }

    private void trainNetwork() {
        if (!training) {
            return;
        }
                
        double output;
        double target;

        for (int i = 0 ; i < ITERATIONS_PER_TRAINING ; i++) {
            System.out.println("Starting training epoch " + i);
            
            int counter = 0;
            double averageChange = 0;
            for (LearningPattern s : data) {
                if (s.getCurState() == null || s.getPrevState() == null) {
                    continue;
                }
                target = s.getReward();
                
                debugPrint("Target before addition " + target);
                
                if (target > 0) {
                    if (RaceMain.sarsa)
                        target += FACTOR_FOR_FUTURE * sarsaStep(s.getCurState());
                    else 
                        target += FACTOR_FOR_FUTURE * qLearningStep(s.getCurState());
                }
                
                debugPrint("Target after " + target);
                
               
                debugPrint("OUTPUT");
                output = runNetwork(s.getPrevState(), s.getChosenAction());
                
                debugPrint("OUTPUT DONE");
                
                double difference = Math.abs(target - output);;
                
                debugPrint("UPDATING");
                updateWeights(output, target);

                double outputnow = runNetwork(s.getPrevState(), s.getChosenAction());
                averageChange += Math.abs(Math.abs(outputnow - target) - difference);
                 if ( Math.abs(outputnow - target) < difference) {
                     counter++;
                 }
                 
                
            }
            
            System.out.println("Score improved on " + counter + " of " + data.size());
            if (averageChange / data.size() < 0.001) {
                break;
            }
        }

    }
    
    private double sarsaStep(NeuralState curState) {
        Random r = new Random();
        if (r.nextDouble() < epsilon) {
            return runNetwork(curState, r.nextInt(NeuralNetQLearning.NUM_ACTIONS));
        }      
        int actionIndex = 5;
        double maxVal = runNetwork(curState, actionIndex);
        double val;
        for (int i = 0 ; i < NeuralNetQLearning.NUM_ACTIONS ; i++) {
            val = runNetwork(curState, i);
            NeuralNetQLearning.debugPrint("Testing action "+ i + " Got val " + val);
            if (val > maxVal) {
                NeuralNetQLearning.debugPrint("Improved");
                actionIndex = i;
                maxVal = val;
            }
        }
        return runNetwork(curState, actionIndex);
    }

    private double qLearningStep(NeuralState curState) {
        double max = runNetwork(curState, 0);

        for (int i = 1 ; i < NUM_ACTIONS ; i++) {
            max = Math.max(max, runNetwork(curState, i));
        }
        return max;
    }

    private void updateWeights(double output, double target) {
           
        debugPrint("OUTPUTLAYER TRAINING");
        double[] outputWeightChanges = upDateOutpuWeights(output, target);
        debugPrint("Hidden training");
        double[][] hiddenLayerChanges = upDateHiddenLayer();

        for (int i = 0 ; i < outputLayerWeights.length ; i++) {
            outputLayerWeights[i] += outputWeightChanges[i];
        }

        for (int i = 0 ; i < hiddenLayerWeights.length ; i++) {
            for (int j = 0 ; j < hiddenLayerWeights[i].length ; j++) {
                hiddenLayerWeights[i][j] += hiddenLayerChanges[i][j];
            }
        }

    }

    private double[] upDateOutpuWeights(double output, double target) {
        
        outputDelta = (output - target);

//        debugPrint("Before outputupdate gradient ");
//        debugPrint(neatlyPrint(outputGradients));
//        
//        debugPrint("Before outputCHanges ");
//        debugPrint(neatlyPrint(outputLayerchange));
//        
//        debugPrint("Before outputprevChanges");
//        debugPrint(neatlyPrint(outputLayerPrevchange));
//        
//        debugPrint("Delta " + outputDelta);
        
        double[] outputWeightChange = new double[outputLayerWeights.length];
        double sign;
        double gradient;

        for (int i = 0 ; i < outputWeightChange.length ; i++) {
//            debugPrint("At weight " + i);
            
            
            if (i != outputWeightChange.length - 1) {
                gradient = hiddenLayerOutput[i] * outputDelta;
//                debugPrint("Hidden output " + hiddenLayerOutput[i]);
            }
            else {
                gradient = biasOutput * outputDelta;
            }
//            debugPrint("Gradient " + gradient);
//            debugPrint("Prev gradient " + outputGradients[i]);

            if (outputGradients[i] * gradient > 0) {
            
//                debugPrint("No sign change");
                sign = calculateSign(gradient);
             
//                debugPrint("Value of sign variable " + sign);
//                debugPrint("Change before " + outputLayerchange[i]);
//                debugPrint("Prev change before " + outputLayerPrevchange[i]);
                
                outputLayerchange[i] = Math.min(MAX_WEIGHT_CHANGE, outputLayerchange[i] * etaPlus);
                
                outputLayerPrevchange[i] = sign * outputLayerchange[i];
                
//                debugPrint("Change after " + outputLayerchange[i]);
//                debugPrint("Prev change after " + outputLayerPrevchange[i]);
                
                outputWeightChange[i] = outputLayerPrevchange[i];
            }
            else if (outputGradients[i] * gradient < 0) {
             
//                debugPrint("sign change ");
//                debugPrint("Change before " + outputLayerchange[i]);
                
                outputLayerchange[i] = Math.max(MIN_WEIGHT_CHANGE, outputLayerchange[i] * etaMinus);
                
//                debugPrint("Change after " + outputLayerchange[i]);
                
                outputWeightChange[i] = (-1) * outputLayerPrevchange[i];
                gradient = 0;
            }
            else {
//                debugPrint("Either one was 0");
                
                sign = calculateSign(gradient);
                
//                debugPrint("Val of sign " + sign);
//                debugPrint("Change before " + outputLayerchange[i]);
                
                outputLayerPrevchange[i] = sign * outputLayerchange[i];
                outputWeightChange[i] = outputLayerPrevchange[i];
                
//                debugPrint("weight change " + outputWeightChange[i]);
            }
            outputGradients[i] = gradient;
//            debugPrint("\n");

        }
        
//        debugPrint("After outputupdate gradient ");
//        debugPrint(neatlyPrint(outputGradients));
//        
//        debugPrint("After outputCHanges ");
//        debugPrint(neatlyPrint(outputLayerchange));
//        
//        debugPrint("After outputprevChanges");
//        debugPrint(neatlyPrint(outputLayerPrevchange));
//
//        
//        debugPrint("Current outputchanges");
//        debugPrint(neatlyPrint(outputWeightChange));
//
//        debugPrint("\n\n");
        return outputWeightChange;
    }

    private double[][] upDateHiddenLayer() {
        double gradient;
        double sign;

////        debugPrint("Before update gradients: ");
//        for (int i = 0 ; i < hiddenLayerGradients.length ; i++) {
//            debugPrint("Hidden layer " + i);
//            debugPrint(neatlyPrint(hiddenLayerGradients[i]));
//            
//        }
//        debugPrint("\n");
//        debugPrint("before update changes ");
//        for (int i = 0 ; i < hiddenLayerChange.length ; i++) {
//            debugPrint("Hidden layer " + i);
//            debugPrint(neatlyPrint(hiddenLayerChange[i]));          
//        }
//        debugPrint("\n");
//        debugPrint("before update prev changes ");
//        for (int i = 0 ; i < hiddenLayerPrevChange.length ; i++) {
//            debugPrint("Hidden layer " + i);
//            debugPrint(neatlyPrint(hiddenLayerPrevChange[i]));          
//        }
//        debugPrint("\n");
//        debugPrint("Output delta " + outputDelta);
//        
        double[][] hiddenLayerWeightChanges = new double[hiddenLayerWeights.length][hiddenLayerWeights[0].length];

        for (int i = 0 ; i < hiddenLayerWeights.length ; i++) {
            for (int j = 0 ; j < hiddenLayerWeights[i].length ; j++) {
//                debugPrint("At node " + i + " weight " + j);
                gradient = hiddenLayerOutput[i] * (1 - hiddenLayerOutput[i]) * outputDelta * outputLayerWeights[i];

                if (j != hiddenLayerWeights[i].length - 1) {
                    gradient *= inputLayerOutput[j];
                }
                else {
                    gradient *= biasOutput;
                }
//                debugPrint("Gradient " + gradient);
                
                if (hiddenLayerGradients[i][j] * gradient > 0) {
                    
//                    debugPrint("No sign change");
                    
                    sign = calculateSign(gradient);
//                    debugPrint("Val of sign " + sign) ;
//                    
//                    debugPrint("Change before " + hiddenLayerChange[i][j]);
                    
                    
                    hiddenLayerChange[i][j] = Math.min(MAX_WEIGHT_CHANGE, hiddenLayerChange[i][j] * etaPlus);
                    
//                    debugPrint("Change after " + hiddenLayerChange[i][j]);
                    hiddenLayerPrevChange[i][j] = sign * hiddenLayerChange[i][j];
                    hiddenLayerWeightChanges[i][j] = hiddenLayerPrevChange[i][j];
                    
//                    debugPrint("Weight change " + hiddenLayerWeightChanges[i][j]);
                }
                else if (hiddenLayerGradients[i][j] * gradient < 0) {
                    
//                    debugPrint("Sign change ");
//                    
//                    debugPrint("Change before " +  hiddenLayerChange[i][j]);
                    
                    hiddenLayerChange[i][j] = Math.max(MIN_WEIGHT_CHANGE, hiddenLayerChange[i][j] * etaMinus);
                    
//                    debugPrint("Change after " +  hiddenLayerChange[i][j]);
                    hiddenLayerWeightChanges[i][j] = (-1) * hiddenLayerPrevChange[i][j];
//                    debugPrint("Weight change " + hiddenLayerWeightChanges[i][j]);
                    gradient = 0;
                }
                else {
                    
//                    debugPrint("Either one 0");
                    sign = calculateSign(gradient);
//                    debugPrint("Val of sign " + sign);
//                    debugPrint("Change before " + hiddenLayerPrevChange[i][j]);
                    hiddenLayerPrevChange[i][j] = sign * hiddenLayerChange[i][j];
                    hiddenLayerWeightChanges[i][j] = hiddenLayerPrevChange[i][j];
                    
//                    debugPrint("Change after " + hiddenLayerPrevChange[i][j]);

                }
                hiddenLayerGradients[i][j] = gradient;
//                debugPrint("\n");
            }
        }
        
//        debugPrint("After update gradients: ");
//        for (int i = 0 ; i < hiddenLayerGradients.length ; i++) {
//            debugPrint("Hidden layer " + i);
//            debugPrint(neatlyPrint(hiddenLayerGradients[i]));
//            
//        }
//        debugPrint("\n");
//        
//        debugPrint("After update changes ");
//        for (int i = 0 ; i < hiddenLayerChange.length ; i++) {
//            debugPrint("Hidden layer " + i);
//            debugPrint(neatlyPrint(hiddenLayerChange[i]));          
//        }
//        debugPrint("\n");
//        debugPrint("After update prev changes ");
//        for (int i = 0 ; i < hiddenLayerPrevChange.length ; i++) {
//            debugPrint("Hidden layer " + i);
//            debugPrint(neatlyPrint(hiddenLayerPrevChange[i]));          
//        }
//        debugPrint("\n");
//        debugPrint("After update weight changes ");
//        for (int i = 0 ; i < hiddenLayerWeightChanges.length ; i++) {
//            debugPrint("Hidden layer " + i);
//            debugPrint(neatlyPrint(hiddenLayerWeightChanges[i]));          
//        }
//        debugPrint("\n");
//        
        return hiddenLayerWeightChanges;
    }

    private double calculateSign(double gradient) {
        double sign = gradient;
        if (sign != 0) {
            sign /= Math.abs(sign);
            sign *= (-1);
        }
        return sign;
    }
    
    public static void debugPrint(String s) {
        if (debug) {
            System.out.println(s);
        }
    }
    
    public static String neatlyPrint(double[] toPrint) {
        
        return "";
//        String print = "";
//        for (int i = 0 ; i < toPrint.length ; i++) {
//            print += (String.format("%,.4f", toPrint[i]) + " ");           
//        }
//        return(print);
    }
    
    public static void main(String[] args) {
        NeuralNetQLearning test = new NeuralNetQLearning();
        
        NeuralState prev = new NeuralState(0, 0, 400, 45, 50, 0, 0, false, false);
        NeuralState cur = new NeuralState(0.5, 1, 350, 50, 45, 23, 25, false, false);
        
        LearningPattern l = new LearningPattern(prev, 5, 3, cur);
        
        NeuralState prev2 = cur;
        NeuralState cur2 = new NeuralState(1, 35, 300, 35, 100, 23, 25, false, false);
        
        LearningPattern l2 = new LearningPattern(prev2, 3, 1, cur2);
        
        test.addLearningPattern(l);
        test.addLearningPattern(l2);
        test.trainNetwork();
        
        
        
        
    }

}
