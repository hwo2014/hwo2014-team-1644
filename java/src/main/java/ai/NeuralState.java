/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai;

public class NeuralState {

    private final double speed;
    private final double angle;
    private final double distanceToNextCurve;
    private final double angleOfNextCurve;
    private final double radiusOfNextCurve;
    private final boolean differentAngleSign;
    private final double curAngle; 
    private final double curRadius;
    private final boolean turbo;
    private final boolean inTurbo;

    public NeuralState( double speed, double angleofCar, 
                        double distanceToNextCurve, double angleOfNextCurve,
                        double radiusofNextCurve, double curAngl, double curRadius,
                        boolean turboAv, boolean inTurbo) {
        this.speed = Math.max(speed, 0);
        this.angle = Math.abs(angleofCar);
        this.distanceToNextCurve =  Math.max(0, distanceToNextCurve);
        this.angleOfNextCurve = Math.abs(angleOfNextCurve);
        this.radiusOfNextCurve = Math.max(radiusofNextCurve, 0);
        this.differentAngleSign = (angleofCar < 0 && curAngl > 0) || (angleofCar > 0 && curAngl < 0);
        this.curAngle = Math.abs(curAngl);
        this.curRadius = curRadius;
        this.turbo = turboAv;
        this.inTurbo = inTurbo;
    }

    public double[] summarize() {
        double[] toReturn = new double[NeuralNetQLearning.NUM_INPUT_NEURONS];
        toReturn[0] = speed;
        toReturn[1] = angle * 0.1;
        toReturn[2] = curAngle * 0.1;
        toReturn[3] = curRadius * 0.01;
        toReturn[4] = differentAngleSign ? 1 : 0;
        toReturn[6] = angleOfNextCurve * 0.1;
        toReturn[7] = radiusOfNextCurve * 0.01;
        toReturn[8] = distanceToNextCurve * 0.01;   
        return toReturn;
    }

    public boolean turboAvailable() {
        return turbo;
    }
    
    
    public double getCarSpeed() {
        return speed;
    }

    public double getCarAngle() {
        return angle;
    }

    public double getDistanceToNextCurve() {
        return distanceToNextCurve;
    }

    public double getAngleOfNextCurve() {
        return angleOfNextCurve;
    }

    public double getRadiusOfNextCurve() {
        return radiusOfNextCurve;
    }

    public boolean isDifferentAngleSign() {
        return differentAngleSign;
    }

    public double getCurPieceAngle() {
        return curAngle;
    }

    public double getCurPieceRadius() {
        return curRadius;
    }
        
    @Override
    public String toString() {
        return ("Speed: " + speed + " \n "
                + "Angle of car " + angle+ " \n "
                + "Distance to Curve " + distanceToNextCurve + " \n "
                + "Angle of Next Curve " + angleOfNextCurve +  " \n "
                + "Radius of Next Curve " + radiusOfNextCurve+  " \n "
                + "Angle of current curve " + curAngle+  " \n "
                + "Radius of current Curve " + curRadius+  " \n "
                + "Turbo available " + turbo +  " \n " 
                + "In turbo  " + inTurbo +  " \n "
                );
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (int)(Double.doubleToLongBits(this.speed) ^ (Double.doubleToLongBits(this.speed) >>> 32));
        hash = 61 * hash + (int)(Double.doubleToLongBits(this.angle) ^ (Double.doubleToLongBits(this.angle) >>> 32));
        hash = 61 * hash + (int)(Double.doubleToLongBits(this.distanceToNextCurve) ^ (Double.doubleToLongBits(this.distanceToNextCurve) >>> 32));
        hash = 61 * hash + (int)(Double.doubleToLongBits(this.angleOfNextCurve) ^ (Double.doubleToLongBits(this.angleOfNextCurve) >>> 32));
        hash = 61 * hash + (int)(Double.doubleToLongBits(this.radiusOfNextCurve) ^ (Double.doubleToLongBits(this.radiusOfNextCurve) >>> 32));
        hash = 61 * hash + (this.differentAngleSign ? 1 : 0);
        hash = 61 * hash + (int)(Double.doubleToLongBits(this.curAngle) ^ (Double.doubleToLongBits(this.curAngle) >>> 32));
        hash = 61 * hash + (int)(Double.doubleToLongBits(this.curRadius) ^ (Double.doubleToLongBits(this.curRadius) >>> 32));
        hash = 61 * hash + (this.turbo ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NeuralState other = (NeuralState)obj;
        if (Double.doubleToLongBits(this.speed) != Double.doubleToLongBits(other.speed)) {
            return false;
        }
        if (Double.doubleToLongBits(this.angle) != Double.doubleToLongBits(other.angle)) {
            return false;
        }
        if (Double.doubleToLongBits(this.distanceToNextCurve) != Double.doubleToLongBits(other.distanceToNextCurve)) {
            return false;
        }
        if (Double.doubleToLongBits(this.angleOfNextCurve) != Double.doubleToLongBits(other.angleOfNextCurve)) {
            return false;
        }
        if (Double.doubleToLongBits(this.radiusOfNextCurve) != Double.doubleToLongBits(other.radiusOfNextCurve)) {
            return false;
        }
        if (this.differentAngleSign != other.differentAngleSign) {
            return false;
        }
        if (Double.doubleToLongBits(this.curAngle) != Double.doubleToLongBits(other.curAngle)) {
            return false;
        }
        if (Double.doubleToLongBits(this.curRadius) != Double.doubleToLongBits(other.curRadius)) {
            return false;
        }
        if (this.turbo != other.turbo) {
            return false;
        }
        return true;
    }


    
    
    
    

}
