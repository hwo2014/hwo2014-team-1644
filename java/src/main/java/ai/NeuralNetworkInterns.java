/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ai;


import java.io.Serializable;

public class NeuralNetworkInterns implements Serializable {
    
     // All weights definied as the weights in to a node
    private final double[][] hiddenLayerWeights;
    private final double[] outputLayerWeights;
    
    private final double[][] hiddenLayerGradients;
    private final double[] outputGradients;
    
    private final double[][] hiddenLayerChange;
    private final double[] outputLayerchange;

    // Weight last changes
    private final double[][] hiddenLayerPrevChange;
    private final double[] outputLayerPrevchange;
    
    private final int lifeTime;

    public NeuralNetworkInterns(double[][] hiddenLayerWeights, double[] outputLayerWeights, double[][] hiddenLayerGradients, double[] outputGradients, double[][] hiddenLayerChange, double[] outputLayerchange, double[][] hiddenLayerPrevChange, double[] outputLayerPrevchange, int lifeTime) {
        this.hiddenLayerWeights = hiddenLayerWeights;
        this.outputLayerWeights = outputLayerWeights;
        this.hiddenLayerGradients = hiddenLayerGradients;
        this.outputGradients = outputGradients;
        this.hiddenLayerChange = hiddenLayerChange;
        this.outputLayerchange = outputLayerchange;
        this.hiddenLayerPrevChange = hiddenLayerPrevChange;
        this.outputLayerPrevchange = outputLayerPrevchange;
        this.lifeTime = lifeTime;
    }

    public double[][] getHiddenLayerWeights() {
        return hiddenLayerWeights;
    }

    public double[] getOutputLayerWeights() {
        return outputLayerWeights;
    }

    public double[][] getHiddenLayerGradients() {
        return hiddenLayerGradients;
    }

    public double[] getOutputGradients() {
        return outputGradients;
    }

    public double[][] getHiddenLayerChange() {
        return hiddenLayerChange;
    }

    public double[] getOutputLayerchange() {
        return outputLayerchange;
    }

    public double[][] getHiddenLayerPrevChange() {
        return hiddenLayerPrevChange;
    }

    public double[] getOutputLayerPrevchange() {
        return outputLayerPrevchange;
    }

    public int getLifeTime() {
        return lifeTime;
    }

    
    
    
    
    
}
