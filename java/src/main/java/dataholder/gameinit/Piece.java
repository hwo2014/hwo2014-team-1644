package dataholder.gameinit;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Lasse Lybeck
 */
public class Piece {

    private double length;
    @SerializedName("switch")
    private boolean switchLane;
    private int radius;
    private double angle;
    private PieceType type;

    public Piece() {
    }

    public Piece(Piece piece) {
        this.length = piece.length;
        this.switchLane = piece.switchLane;
        this.radius = piece.radius;
        this.angle = piece.angle;
        this.type = piece.type;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public boolean isSwitch() {
        return switchLane;
    }

    public void setSwitch(boolean switchLane) {
        this.switchLane = switchLane;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public PieceType getType() {
        return type;
    }

    public void setType(PieceType type) {
        this.type = type;
    }

    public enum PieceType {

        STRAIGHT, CURVE;
    }
}
