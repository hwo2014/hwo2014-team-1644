package dataholder.gameinit;

import dataholder.carpositions.Id;

/**
 *
 * @author Lasse Lybeck
 */
public class Car {

    private Id id;
    private Dimensions dimensions;

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

    public void setDimensions(Dimensions dimensions) {
        this.dimensions = dimensions;
    }

}
