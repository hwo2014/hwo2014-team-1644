package dataholder.gameinit;

/**
 *
 * @author Lasse Lybeck
 */
public class StartingPoint {

    private Position position;
    private double angle;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }
}
