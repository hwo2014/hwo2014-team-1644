package dataholder.gameinit;

import java.util.List;

/**
 *
 * @author Lasse Lybeck
 */
public class Track {

    private String id;
    private String name;
    private List<Piece> pieces;
    private List<Lane> lanes;
    private StartingPoint startingPoint;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Piece> getPieces() {
        return pieces;
    }

    public void setPieces(List<Piece> pieces) {
        this.pieces = pieces;
    }

    public List<Lane> getLanes() {
        return lanes;
    }

    public void setLanes(List<Lane> lanes) {
        this.lanes = lanes;
    }

    public StartingPoint getStartingPoint() {
        return startingPoint;
    }

    public void setStartingPoint(StartingPoint startingPoint) {
        this.startingPoint = startingPoint;
    }

    private boolean isSameCurve(Piece lastPiece, Piece thisPiece) {
        return lastPiece.getRadius() == thisPiece.getRadius() && isSameSign(lastPiece.getAngle(), thisPiece.getAngle());
    }

    private boolean isSameSign(double a1, double a2) {
        return a1 * a2 > 0;
    }

    private boolean isBothStraight(Piece lastPiece, Piece thisPiece) {
        return lastPiece.getType() == Piece.PieceType.STRAIGHT && thisPiece.getType() == Piece.PieceType.STRAIGHT;
    }
}
