package dataholder.gameinit;

/**
 *
 * @author Lasse Lybeck
 */
public class MergedPieceData {

    private double offset;
    private Piece piece;

    public double getOffset() {
        return offset;
    }

    public void setOffset(double offset) {
        this.offset = offset;
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }
}
