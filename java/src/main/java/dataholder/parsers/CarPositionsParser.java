package dataholder.parsers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import dataholder.carpositions.CarPosition;
import dataholder.carpositions.Id;
import dataholder.carpositions.Lane;
import dataholder.carpositions.PiecePosition;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lasse Lybeck
 */
public class CarPositionsParser {

    public static List<CarPosition> parseCarPositions(JsonElement data) {
        JsonArray jsonArray = data.getAsJsonArray();
        List<CarPosition> carPositionsList = new ArrayList<>(jsonArray.size());
        for (JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            CarPosition carPosition = new CarPosition();
            carPosition.setId(parseId(jsonObject.get("id")));
            carPosition.setAngle(jsonObject.get("angle").getAsDouble());
            carPosition.setPiecePosition(parsePiecePosition(jsonObject.get("piecePosition")));
            carPositionsList.add(carPosition);
        }
        return carPositionsList;
    }

    protected static Id parseId(JsonElement elem) {
        JsonObject jsonObject = elem.getAsJsonObject();
        Id id = new Id();
        id.setName(jsonObject.get("name").getAsString());
        id.setColor(jsonObject.get("color").getAsString());
        return id;
    }

    private static PiecePosition parsePiecePosition(JsonElement elem) {
        JsonObject jsonObject = elem.getAsJsonObject();
        PiecePosition piecePosition = new PiecePosition();
        piecePosition.setPieceIndex(jsonObject.get("pieceIndex").getAsInt());
        piecePosition.setInPieceDistance(jsonObject.get("inPieceDistance").getAsDouble());
        piecePosition.setLap(jsonObject.get("lap").getAsInt());
        piecePosition.setLane(parseLane(jsonObject.get("lane")));
        return piecePosition;
    }

    private static Lane parseLane(JsonElement elem) {
        JsonObject jsonObject = elem.getAsJsonObject();
        Lane lane = new Lane();
        lane.setStartLaneIndex(jsonObject.get("startLaneIndex").getAsInt());
        lane.setEndLaneIndex(jsonObject.get("endLaneIndex").getAsInt());
        return lane;
    }
}
