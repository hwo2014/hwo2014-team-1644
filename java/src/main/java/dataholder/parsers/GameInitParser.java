/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataholder.parsers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import dataholder.gameinit.Car;
import dataholder.gameinit.Dimensions;
import dataholder.gameinit.Lane;
import dataholder.gameinit.Piece;
import dataholder.gameinit.Position;
import dataholder.gameinit.Race;
import dataholder.gameinit.RaceSession;
import dataholder.gameinit.StartingPoint;
import dataholder.gameinit.Track;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Christoffer
 */
public class GameInitParser {

    public static Race parseGameInit(JsonElement elem) {
        JsonObject jsonObject = elem.getAsJsonObject().get("race").getAsJsonObject();
        Race race = new Race();
        race.setTrack(parseTrack(jsonObject.get("track")));
        race.setCars(parseCars(jsonObject.get("cars")));
        race.setRaceSession(parseRaceSession(jsonObject.get("raceSession")));
        return race;
    }

    private static Track parseTrack(JsonElement elem) {
        JsonObject jsonObject = elem.getAsJsonObject();
        Track track = new Track();
        track.setId(jsonObject.get("id").getAsString());
        track.setName(jsonObject.get("name").getAsString());
        track.setPieces(parsePieces(jsonObject.get("pieces")));
        track.setLanes(parseLanes(jsonObject.get("lanes")));
        track.setStartingPoint(parseStartingPoint(jsonObject.get("startingPoint")));
        return track;
    }

    private static List<Piece> parsePieces(JsonElement elem) {
        JsonArray jsonArray = elem.getAsJsonArray();
        List<Piece> piecesList = new ArrayList<>(jsonArray.size());
        for (JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            Piece piece = new Piece();
            JsonElement lengthElement = jsonObject.get("length");
            if (lengthElement != null) {
                piece.setLength(lengthElement.getAsDouble());
                piece.setType(Piece.PieceType.STRAIGHT);
            }
            JsonElement switchElement = jsonObject.get("switch");
            if (switchElement != null) {
                piece.setSwitch(switchElement.getAsBoolean());
            }
            JsonElement radiusElement = jsonObject.get("radius");
            JsonElement angleElement = jsonObject.get("angle");
            if (radiusElement != null && angleElement != null) {
                int radius = radiusElement.getAsInt();
                double angle = angleElement.getAsDouble();
                double length = Math.abs(Math.toRadians(angle)) * radius;
                piece.setRadius(radius);
                piece.setAngle(angle);
                piece.setLength(length);
                piece.setType(Piece.PieceType.CURVE);
            }
            piecesList.add(piece);
        }
        return piecesList;
    }

    private static List<Lane> parseLanes(JsonElement elem) {
        JsonArray jsonArray = elem.getAsJsonArray();
        List<Lane> laneList = new ArrayList<>(jsonArray.size());
        for (JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            Lane lane = new Lane();
            lane.setDistanceFromCenter(jsonObject.get("distanceFromCenter").getAsInt());
            lane.setIndex(jsonObject.get("index").getAsInt());
            laneList.add(lane);
        }
        return laneList;
    }

    private static StartingPoint parseStartingPoint(JsonElement elem) {
        JsonObject jsonObject = elem.getAsJsonObject();
        StartingPoint startingPoint = new StartingPoint();
        startingPoint.setPosition(parsePosition(jsonObject.get("position")));
        startingPoint.setAngle(jsonObject.get("angle").getAsDouble());
        return startingPoint;
    }

    private static Position parsePosition(JsonElement elem) {
        JsonObject jsonObject = elem.getAsJsonObject();
        Position position = new Position();
        position.setX(jsonObject.get("x").getAsDouble());
        position.setY(jsonObject.get("y").getAsDouble());
        return position;
    }

    private static List<Car> parseCars(JsonElement elem) {
        JsonArray jsonArray = elem.getAsJsonArray();
        List<Car> carList = new ArrayList<>(jsonArray.size());
        for (JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            Car car = new Car();
            car.setId(CarPositionsParser.parseId(jsonObject.get("id")));
            car.setDimensions(parseDimensions(jsonObject.get("dimensions")));
            carList.add(car);
        }
        return carList;
    }

    private static Dimensions parseDimensions(JsonElement elem) {
        JsonObject jsonObject = elem.getAsJsonObject();
        Dimensions dimensions = new Dimensions();
        dimensions.setLength(jsonObject.get("length").getAsDouble());
        dimensions.setWidth(jsonObject.get("width").getAsDouble());
        dimensions.setGuideFlagPosition(jsonObject.get("guideFlagPosition").getAsDouble());
        return dimensions;
    }

    private static RaceSession parseRaceSession(JsonElement elem) {
        JsonObject jsonObject = elem.getAsJsonObject();
        RaceSession raceSession = new RaceSession();
        if (jsonObject.get("laps") != null)
            raceSession.setLaps(jsonObject.get("laps").getAsInt());
        if (jsonObject.get("maxLapTimeMs") != null)
            raceSession.setMaxLapTimeMs(jsonObject.get("maxLapTimeMs").getAsLong());
        if (jsonObject.get("quickRace") != null)
            raceSession.setQuickRace(jsonObject.get("quickRace").getAsBoolean());
        return raceSession;
    }
}
