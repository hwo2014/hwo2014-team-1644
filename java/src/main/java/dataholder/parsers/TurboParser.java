package dataholder.parsers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import dataholder.turbo.Turbo;

/**
 *
 * @author Lasse Lybeck
 */
public class TurboParser {

    public static Turbo parseTurbo(JsonElement elem) {
        JsonObject jsonObject = elem.getAsJsonObject();
        Turbo turbo = new Turbo();
        JsonElement millisElem = jsonObject.get("turboDurationMilliseconds");
        if (millisElem != null) {
            turbo.setTurboDurationMilliseconds(millisElem.getAsDouble());
        }
        JsonElement tickElem = jsonObject.get("turboDurationTicks");
        if (tickElem != null) {
            turbo.setTurboDurationTicks(tickElem.getAsInt());
        }
        JsonElement factorElem = jsonObject.get("turboFactor");
        if (factorElem != null) {
            turbo.setTurboFactor(factorElem.getAsDouble());
        }
        return turbo;
    }
}
