/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataholder.parsers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import dataholder.carpositions.Id;

/**
 *
 * @author Christoffer
 */
public class IdParser {

    public static Id parseId(JsonElement data) {
        JsonObject jsonObject = data.getAsJsonObject();
        Id yourCar = new Id();
        yourCar.setName(jsonObject.get("name").getAsString());
        yourCar.setColor(jsonObject.get("color").getAsString());
        return yourCar;
    }
}
