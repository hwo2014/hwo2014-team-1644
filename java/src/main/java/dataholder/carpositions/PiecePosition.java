package dataholder.carpositions;

/**
 *
 * @author Lasse Lybeck
 */
public class PiecePosition {

    private int pieceIndex;
    private double inPieceDistance;
    private Lane lane;
    private int lap;

    public int getPieceIndex() {
        return pieceIndex;
    }

    public void setPieceIndex(int pieceIndex) {
        this.pieceIndex = pieceIndex;
    }

    public double getInPieceDistance() {
        return inPieceDistance;
    }

    public void setInPieceDistance(double inPieceDistance) {
        this.inPieceDistance = inPieceDistance;
    }

    public Lane getLane() {
        return lane;
    }

    public void setLane(Lane lane) {
        this.lane = lane;
    }

    public int getLap() {
        return lap;
    }

    public void setLap(int lap) {
        this.lap = lap;
    }
}
