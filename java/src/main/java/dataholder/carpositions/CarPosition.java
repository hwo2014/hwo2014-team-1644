package dataholder.carpositions;

/**
 *
 * @author Lasse Lybeck
 */
public class CarPosition {

    private Id id;
    private double angle;
    private PiecePosition piecePosition;

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public PiecePosition getPiecePosition() {
        return piecePosition;
    }

    public void setPiecePosition(PiecePosition piecePosition) {
        this.piecePosition = piecePosition;
    }

}
