package dataholder.carpositions;

import java.util.Objects;

/**
 *
 * @author Lasse Lybeck
 */
public class Id {

    private String name;
    private String color;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Id)) {
            return false;
        }
        Id c = (Id) obj;
        return this.name.equals(c.name) && this.color.equals(c.color);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.name);
        hash = 53 * hash + Objects.hashCode(this.color);
        return hash;
    }
}
