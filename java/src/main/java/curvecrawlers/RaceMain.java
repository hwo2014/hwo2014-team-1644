package curvecrawlers;

import ai.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author Lasse Lybeck
 */
public class RaceMain {

    public static boolean sarsa = false;
    
    private static final String[] tracks = {"keimola", "imola", "suzuka", "england", "elaeintarha", "germany", "france", "usa"};

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName;
        String qLearningFileName;
        if (sarsa) {
            botName = "Curve SARSA";
            qLearningFileName = "SARSA.dat";
        } else {
            qLearningFileName = "Qlearn.dat";
            botName = qLearningFileName; // args[2] +"E5";
        }

        
        String botKey = args[3];
        NeuralNetworkInterns brains;

        System.out.println("Connecting to " + host + ":" + port + " as '" + botName + "' with key'" + botKey + "'");
        Serialiser<NeuralNetworkInterns> s = new Serialiser<>();

        if (new File(qLearningFileName).isFile()) {
            System.out.println("Found old Brains");
            brains = s.deserialize(qLearningFileName);
        }
        else {
            System.out.println("No brains found");
            brains = null;
        }

        NeuralNetQLearning ai;
        System.out.println("Initializing AI");
        if (brains == null) {
            ai = new NeuralNetQLearning();
        }
        else {
            ai = new NeuralNetQLearning(brains);
        }

        for (int i = 0 ; i < 1 ; i++) {
            System.out.println("STARTING RACE " + i);
            System.out.println("SARSA: " + sarsa);
            System.out.println("Num races: " + ai.getLifeTime());
            String track = "keimola";
            System.out.println("Racing on " + track);
            new NeuralQLearning(host, port, botName, botKey, ai).race(track, "niusnfiubr", 1);
            ai.addLifeTime();
            if (ai.getLifeTime() % 5 == 0) {
                System.out.println("SERIALIZING");
                s.serialize(ai.getWeights(), qLearningFileName + ai.getLifeTime());
            }
        }

        System.out.println("AFTER RACES");
        ai.summarizeNetwork();
        
        s.serialize(ai.getWeights(), qLearningFileName + ai.getLifeTime());

//        double BUCKETSIZE_DISTANCE_TO_CURVE,
//        double BUCKETSIZE_ANGLE_OF_NEXT_CURVE,
//        double BUCKETSIZE_SPEED,
//        double BUCKETSIZE_ANGLE,
//        double BUCKETSIZE_RADIUS,
//        double BASE_FOR_REWARD,
//        int NUM_OF_ACTIONS) {
//        for (int i = 0 ; i < 50 ; i++) {
//            new QLearning(host, port, botName, botKey, brains).race();
//        new QLearning(host, port, botName, botKey, brains).race("france", "niusnfiubr", 1);       
    }

}
