package curvecrawlers;

import ai.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author Lasse Lybeck
 */
public class CompetitionMain {


    private static final String qLearningFileName = "src/main/resources/Qlearn.dat";
    
    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        
        System.out.println("Connecting to " + host + ":" + port + " as '" + botName + "' with key'" + botKey + "'");
        
       
        Serialiser<NeuralNetworkInterns> s = new Serialiser<>();
        NeuralNetworkInterns brains; 
        if (new File(qLearningFileName).isFile()) {
            System.out.println("Found old Brains");
            brains = s.deserialize(qLearningFileName);
        }
        else {
            System.out.println("No brains found");
            brains = null;
        }

        NeuralNetQLearning ai;
        System.out.println("Initializing AI");
        if (brains == null) {
            ai = new NeuralNetQLearning();
        }
        else {
            ai = new NeuralNetQLearning(brains);
        }
                   
        new NeuralQLearning(host, port, botName, botKey, ai).race();
        
    }
}
