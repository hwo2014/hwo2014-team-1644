package curvecrawlers;

import ai.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lasse Lybeck
 */
public class Main {

    public static int configuration = 2;
    public static final String tracksFileName = "Tracks.dat";

    public final static String PASSWORD = "SombrerO_BOYs_WithOuT_HATs";
    private static ArrayList<String> tracks;
    //= {"keimola", "germany"};

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        
        
        selectQLearningConfiguration(args);
        String qLearningFileName = "/tmp/curvecrawlers/QLearner" + configuration + ".dat";

        readInTracks();

        int trackIndex = 0;
        int carCount = 1;
        if (args.length > 4) {
            try {
                trackIndex = Integer.parseInt(args[4]);
            } catch (NumberFormatException ex) {
                System.out.println("Argument needs to be an Integer");
                System.err.println(ex.getMessage());
            }

            if (trackIndex > tracks.size()) {
                System.out.println("Choose a track index between: 0 - " + (tracks.size() - 1));
                trackIndex = 0;
            }
        }
        if (args.length > 5) {
            try {
                carCount = Integer.parseInt(args[5]);
            } catch (NumberFormatException ex) {
                System.out.println("Argument needs to be an Integer");
                System.err.println(ex.getMessage());
            }

            if (carCount < 1) {
                System.out.println("Must be at least one car!");
                carCount = 1;
            }
        }

        System.out.println("Connecting to " + host + ":" + port + " as '" + botName + "' with key'" + botKey + "'");
        
        
        QLearning curAI = null;
        int numActions = 11;
        QLearnerBrains brains = new QLearnerBrains(15, 5, 0.5, 3, 10, 3, numActions);
        Serialiser<QLearnerBrains> s = new Serialiser<>();

        if (new File(qLearningFileName).isFile()) {
            System.out.println("Found old Brains");
            brains = s.deserialize(qLearningFileName);
        } else {
            System.out.println("No brains found");
            if (configuration <= 1) 
//                double BUCKETSIZE_DISTANCE_TO_CURVE, 
//                        double BUCKETSIZE_ANGLE_OF_NEXT_CURVE, 
//                                double BUCKETSIZE_SPEED, 
//                                        double BUCKETSIZE_ANGLE, 
//                                                double BUCKETSIZE_RADIUS, 
//                                                        double BASE_FOR_REWARD, 
//                                                                int NUM_OF_ACTIONS
                brains = new QLearnerBrains(15, 5, 0.5, 3, 10, 3, numActions);
            if (configuration == 2)
                brains = new QLearnerBrains(15, 5, 0.5, 3, 10, 4, numActions);
            if (configuration == 3)
                brains = new QLearnerBrains(10, 5, 0.5, 2, 5, 3, numActions);
            if (configuration == 4)
                brains = new QLearnerBrains(10, 5, 0.5, 2, 5, 4, numActions);
            if (configuration == 5)
                brains = new QLearnerBrains(20, 10, 0.6, 1.5, 15, 3, numActions);
            if (configuration >= 6)
                brains = new QLearnerBrains(20, 10, 0.6, 1.5, 15, 4, numActions);
        }

        int tracksNo = Integer.MAX_VALUE; // all tracks
        int racesPerTrack = 2;
        
        tracksNo = Math.min(tracksNo, tracks.size());
        int raceCounter = 1;
        int totalRaces = racesPerTrack * tracksNo;
        for (int i = 0; i < tracksNo; ++i) {
            String track = tracks.get(i);
            for (int j = 0; j < racesPerTrack; j++) {

//                curAI = new QLearning(host, port, botName, botKey, brains);
//                brains.restartLearning();

                System.out.println("--------------------------------");
                System.out.println("Starting race " + (raceCounter++) + " of " + totalRaces + ".");
                System.out.println("--------------------------------");

//                curAI.race(track, null, carCount, args.length > 6 && "yes".equalsIgnoreCase(args[6]));
//                brains = curAI.getBrains();
            }
        }

//        if (trackIndex < 0) {
//            System.out.println("\tTrack: " + tracks.get(brains.getNumRaces() % tracks.size()));
//            trackIndex = brains.getNumRaces() % (tracks.size());
//            curAI.race(tracks.get(trackIndex), PASSWORD, carCount);
//        } else {
//           System.out.println("index of track " + trackIndex);
//            curAI.race(tracks.get(trackIndex), null, carCount, args.length > 6 && "yes".equalsIgnoreCase(args[6]));
//        }

        System.out.println("Serializing brains");
            s.serialize(curAI.getBrains(), qLearningFileName);
    }

    private static void selectQLearningConfiguration(String[] args) {
        if(args.length > 7){
            try{
                configuration = Integer.parseInt(args[7]);
            } catch (NumberFormatException ex){
                System.out.println("Argument needs to be an Integer");
                System.err.println(ex.getMessage());
                configuration = 1;
            }
            System.out.println("Selecting configuration: " + configuration);
        }
    }

    private static void readInTracks() {
        tracks = new ArrayList<>();
        File file = new File(tracksFileName);
        if (!file.exists()) {
            System.err.println(tracksFileName + " not found!");
            System.out.println("Using default track!");
            tracks.add("keimola");
        } else {
            System.out.println("Reading tracks from: " + tracksFileName);
            try {
                Scanner scanner = new Scanner(file);
                while (scanner.hasNext()) {
                    tracks.add(scanner.next());
                }

            } catch (FileNotFoundException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
