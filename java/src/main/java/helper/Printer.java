package helper;

import com.google.gson.Gson;

/**
 *
 * @author Lasse Lybeck
 */
public class Printer {

    private static Gson gson = new Gson();

    public static void printAsJson(Object o) {
        printAsJson(null, o);
    }

    public static void printAsJson(String name, Object o) {
        String jsonString = gson.toJson(o);
        if (name != null) {
            System.out.print(name + " = ");
        }
        System.out.println(jsonString);
    }
}
